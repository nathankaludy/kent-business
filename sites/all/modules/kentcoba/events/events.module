<?php

function events_form_alter(&$form, &$form_state, $form_id) {
	switch($form_id) {
		case 'event_node_form':
			$form['#validate'][] = 'events_event_node_form_validate';
			break;
	}
}

function events_event_node_form_validate($form, &$form_state) {
	$start_field = $form_state['values']['ks_start_date'][LANGUAGE_NONE][0]['value'];

	$start_date = strtotime($start_field);

	if (empty($start_field) || false === $start_date) {
		form_set_error('ks_start_date', "Start date is required.");
	}
}

function events_views_api() {
	return array('api' => 3.0);
}

function events_views_query_alter(&$view, &$query) {
	if ($view->name == 'events') {
		if (isset($_REQUEST['monday'])) {
			$monday = (strtotime($_REQUEST['monday'])) ? new DateTime($_REQUEST['monday']) : new DateTime();
			$sunday = clone $monday;
			$sunday->modify('+6 days');
			$query->add_where(0, 'DATE(field_data_ks_start_date.ks_start_date_value) >= :start_date', array(':start_date' => $monday->format('Y-m-d')), 'formula');
			$query->add_where(0, 'DATE(field_data_ks_start_date.ks_start_date_value2) <= :end_date', array(':end_date' => $sunday->format('Y-m-d')), 'formula');

			$view->start_date = $monday;
			$view->end_date = $sunday;
		} else if (!isset($view->argument['ks_start_date_value']->argument)) {
			$sunday = new DateTime('Sunday this week');
			$monday = new DateTime('Monday this week');
			$start_date = min($sunday, $monday);
			$end_date = max($sunday, $monday);
			$query->add_where(0, 'DATE(field_data_ks_start_date.ks_start_date_value) >= :start_date', array(':start_date' => $start_date->format('Y-m-d')), 'formula');
			$query->add_where(0, 'DATE(field_data_ks_start_date.ks_start_date_value2) <= :end_date', array(':end_date' => $end_date->format('Y-m-d')), 'formula');

			$view->start_date = $start_date;
			$view->end_date = $end_date;
		}
	}
}

function events_views_pre_render(&$view) {
	if ($view->name == 'events') {
		$results = array();
		foreach ($view->result as $result) {
			$dates = ksGetSaneStartEndDate($result->field_ks_start_date[0]['raw']);
			$start = $dates['start'];
			$end   = $dates['end'];
			$clean = array(
				'nid'        => $result->nid,
				'title'      => $result->node_title,
				'location'   => $result->field_ks_location[0]['raw']['value'],
				'start_date' => max($start, $view->start_date),
				'end_date'   => min($end, $view->end_date),
				'timestamp'  => $start->format('U'),
				'image'      => isset($result->file_managed_field_data_ks_image_uri) ? file_create_url($result->file_managed_field_data_ks_image_uri) : '',
				'featured'   => isset($result->field_ks_is_featured) ? $result->field_ks_is_featured[0]['raw']['value'] : 0,
			);
			$expanded = eventsSplitDates($clean);
			$results = array_merge($results, $expanded);
		}

		$results = sortByValue($results, 'timestamp', SORT_ASC);
		$view->useful = $results;
	}
}

function eventGetSaneDate($date) {
	$dt = new DateTime($date['value'], new DateTimeZone($date['timezone_db']));
	$dt->setTimezone(new DateTimeZone($date['timezone']));

	return $dt;
}

if (!function_exists('ksGetSaneStartEndDate')) {
	function ksGetSaneStartEndDate($date) {
		$start = new DateTime($date['value'], new DateTimeZone($date['timezone_db']));
		$start->setTimezone(new DateTimeZone($date['timezone']));

		if (array_key_exists('value2', $date)) {
			$end = new DateTime($date['value2'], new DateTimeZone($date['timezone_db']));
			$end->setTimezone(new DateTimeZone($date['timezone']));
		} else {
			$end = clone $start;
		}

		return array(
			'start' => $start,
			'end' => $end,
		);
	}
}

function eventsSplitDates($event) {
	$events = array($event);
	// Compare dates, determine number of days between them.
	$interval = $event['end_date']->diff($event['start_date']);
	$days = $interval->days;

	if ($days) {
		for ($i = 1; $i <= $days; $i++) {
			$start = clone $event['start_date'];
			$start->modify("+{$i} days");

			$ne = $event;
			$ne['start_date'] = $start;
			$ne['timestamp'] = $start->format('U');

			$events[] = $ne;
		}
	}

	return $events;
}

/**
 * @param array  $array     Array to sort
 * @param string $sortOn    Key to use for sorting
 * @param int    $direction Direction of sort
 *
 * @return mixed
 */
function sortByValue($array, $sortOn, $direction = SORT_DESC) {
	$sort = array();
	foreach ($array as $key => $value) {
		$sort[$key] = $value[$sortOn];
	}

	$sort = array_map('strtolower', $sort);

	array_multisort($sort, $direction, $array);

	return $array;
}

function events_form_views_exposed_form_alter(&$form, &$form_state, $form_id) {
	$form['#attributes']['data-domid'] = $form_state['view']->dom_id;
}

function events_views_default_views() {
	$view = new view();
	$view->name = 'events';
	$view->description = '';
	$view->tag = 'default';
	$view->base_table = 'node';
	$view->human_name = 'Events';
	$view->core = 7;
	$view->api_version = '3.0';
	$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

	/* Display: Master */
	$handler = $view->new_display('default', 'Master', 'default');
	$handler->display->display_options['title'] = 'Home Page Block';
	$handler->display->display_options['use_ajax'] = TRUE;
	$handler->display->display_options['use_more_always'] = FALSE;
	$handler->display->display_options['access']['type'] = 'perm';
	$handler->display->display_options['cache']['type'] = 'none';
	$handler->display->display_options['query']['type'] = 'views_query';
	$handler->display->display_options['exposed_form']['type'] = 'basic';
	$handler->display->display_options['exposed_form']['options']['autosubmit'] = TRUE;
	$handler->display->display_options['pager']['type'] = 'full';
	$handler->display->display_options['pager']['options']['items_per_page'] = '6';
	$handler->display->display_options['style_plugin'] = 'default';
	$handler->display->display_options['style_options']['default_row_class'] = FALSE;
	$handler->display->display_options['style_options']['row_class_special'] = FALSE;
	$handler->display->display_options['row_plugin'] = 'fields';
	$handler->display->display_options['row_options']['default_field_elements'] = FALSE;
	/* Field: Content: Title */
	$handler->display->display_options['fields']['title']['id'] = 'title';
	$handler->display->display_options['fields']['title']['table'] = 'node';
	$handler->display->display_options['fields']['title']['field'] = 'title';
	$handler->display->display_options['fields']['title']['label'] = '';
	$handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
	$handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
	/* Filter criterion: Content: Published */
	$handler->display->display_options['filters']['status']['id'] = 'status';
	$handler->display->display_options['filters']['status']['table'] = 'node';
	$handler->display->display_options['filters']['status']['field'] = 'status';
	$handler->display->display_options['filters']['status']['value'] = 1;
	$handler->display->display_options['filters']['status']['group'] = 1;
	$handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
	/* Filter criterion: Content: Type */
	$handler->display->display_options['filters']['type']['id'] = 'type';
	$handler->display->display_options['filters']['type']['table'] = 'node';
	$handler->display->display_options['filters']['type']['field'] = 'type';
	$handler->display->display_options['filters']['type']['value'] = array(
	  'event' => 'event',
	);

	/* Display: Home Page Block */
	$handler = $view->new_display('block', 'Home Page Block', 'block');
	$handler->display->display_options['defaults']['title'] = FALSE;
	$handler->display->display_options['title'] = '<none>';
	$handler->display->display_options['defaults']['use_ajax'] = FALSE;
	$handler->display->display_options['use_ajax'] = TRUE;
	$handler->display->display_options['defaults']['pager'] = FALSE;
	$handler->display->display_options['pager']['type'] = 'none';
	$handler->display->display_options['pager']['options']['offset'] = '0';
	$handler->display->display_options['defaults']['fields'] = FALSE;
	/* Field: Content: Start Date */
	$handler->display->display_options['fields']['ks_start_date']['id'] = 'ks_start_date';
	$handler->display->display_options['fields']['ks_start_date']['table'] = 'field_data_ks_start_date';
	$handler->display->display_options['fields']['ks_start_date']['field'] = 'ks_start_date';
	$handler->display->display_options['fields']['ks_start_date']['label'] = '';
	$handler->display->display_options['fields']['ks_start_date']['element_label_colon'] = FALSE;
	$handler->display->display_options['fields']['ks_start_date']['element_default_classes'] = FALSE;
	$handler->display->display_options['fields']['ks_start_date']['hide_alter_empty'] = FALSE;
	$handler->display->display_options['fields']['ks_start_date']['type'] = 'date_plain';
	$handler->display->display_options['fields']['ks_start_date']['settings'] = array(
	  'format_type' => 'long',
	  'fromto' => 'both',
	  'multiple_number' => '',
	  'multiple_from' => '',
	  'multiple_to' => '',
	);
	/* Field: Content: Title */
	$handler->display->display_options['fields']['title']['id'] = 'title';
	$handler->display->display_options['fields']['title']['table'] = 'node';
	$handler->display->display_options['fields']['title']['field'] = 'title';
	$handler->display->display_options['fields']['title']['label'] = '';
	$handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
	$handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
	$handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
	$handler->display->display_options['fields']['title']['element_default_classes'] = FALSE;
	$handler->display->display_options['fields']['title']['hide_alter_empty'] = FALSE;
	$handler->display->display_options['fields']['title']['link_to_node'] = FALSE;
	/* Field: Content: Location */
	$handler->display->display_options['fields']['ks_location']['id'] = 'ks_location';
	$handler->display->display_options['fields']['ks_location']['table'] = 'field_data_ks_location';
	$handler->display->display_options['fields']['ks_location']['field'] = 'ks_location';
	$handler->display->display_options['fields']['ks_location']['label'] = '';
	$handler->display->display_options['fields']['ks_location']['element_label_colon'] = FALSE;
	$handler->display->display_options['fields']['ks_location']['element_default_classes'] = FALSE;
	$handler->display->display_options['fields']['ks_location']['hide_alter_empty'] = FALSE;
	$handler->display->display_options['fields']['ks_location']['type'] = 'text_plain';
	/* Field: Content: Nid */
	$handler->display->display_options['fields']['nid']['id'] = 'nid';
	$handler->display->display_options['fields']['nid']['table'] = 'node';
	$handler->display->display_options['fields']['nid']['field'] = 'nid';
	$handler->display->display_options['fields']['nid']['label'] = '';
	$handler->display->display_options['fields']['nid']['element_label_colon'] = FALSE;
	$handler->display->display_options['fields']['nid']['element_default_classes'] = FALSE;
	$handler->display->display_options['defaults']['sorts'] = FALSE;
	/* Sort criterion: Content: Start Date (ks_start_date) */
	$handler->display->display_options['sorts']['ks_start_date_value']['id'] = 'ks_start_date_value';
	$handler->display->display_options['sorts']['ks_start_date_value']['table'] = 'field_data_ks_start_date';
	$handler->display->display_options['sorts']['ks_start_date_value']['field'] = 'ks_start_date_value';
	$handler->display->display_options['sorts']['ks_start_date_value']['order'] = 'DESC';
	$handler->display->display_options['defaults']['arguments'] = FALSE;
	$handler->display->display_options['defaults']['filter_groups'] = FALSE;
	$handler->display->display_options['defaults']['filters'] = FALSE;
	/* Filter criterion: Content: Published */
	$handler->display->display_options['filters']['status']['id'] = 'status';
	$handler->display->display_options['filters']['status']['table'] = 'node';
	$handler->display->display_options['filters']['status']['field'] = 'status';
	$handler->display->display_options['filters']['status']['value'] = 1;
	$handler->display->display_options['filters']['status']['group'] = 1;
	$handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
	/* Filter criterion: Content: Type */
	$handler->display->display_options['filters']['type']['id'] = 'type';
	$handler->display->display_options['filters']['type']['table'] = 'node';
	$handler->display->display_options['filters']['type']['field'] = 'type';
	$handler->display->display_options['filters']['type']['value'] = array(
	  'event' => 'event',
	);
	$handler->display->display_options['filters']['type']['group'] = 1;
	/* Filter criterion: Content: Department (ks_department) */
	$handler->display->display_options['filters']['ks_department_tid']['id'] = 'ks_department_tid';
	$handler->display->display_options['filters']['ks_department_tid']['table'] = 'field_data_ks_department';
	$handler->display->display_options['filters']['ks_department_tid']['field'] = 'ks_department_tid';
	$handler->display->display_options['filters']['ks_department_tid']['value'] = array(
	  7 => '7',
	  8 => '8',
	  9 => '9',
	  11 => '11',
	  10 => '10',
	);
	$handler->display->display_options['filters']['ks_department_tid']['group'] = 1;
	$handler->display->display_options['filters']['ks_department_tid']['exposed'] = TRUE;
	$handler->display->display_options['filters']['ks_department_tid']['expose']['operator_id'] = 'ks_department_tid_op';
	$handler->display->display_options['filters']['ks_department_tid']['expose']['operator'] = 'ks_department_tid_op';
	$handler->display->display_options['filters']['ks_department_tid']['expose']['identifier'] = 'ks_department_tid';
	$handler->display->display_options['filters']['ks_department_tid']['expose']['remember_roles'] = array(
	  2 => '2',
	  1 => '1',
	  3 => '3',
	);
	$handler->display->display_options['filters']['ks_department_tid']['expose']['reduce'] = TRUE;
	$handler->display->display_options['filters']['ks_department_tid']['type'] = 'select';
	$handler->display->display_options['filters']['ks_department_tid']['vocabulary'] = 'ks_departments';
	$handler->display->display_options['filters']['ks_department_tid']['error_message'] = FALSE;
	/* Filter criterion: Content: Department (ks_department) */
	$handler->display->display_options['filters']['ks_department_tid_1']['id'] = 'ks_department_tid_1';
	$handler->display->display_options['filters']['ks_department_tid_1']['table'] = 'field_data_ks_department';
	$handler->display->display_options['filters']['ks_department_tid_1']['field'] = 'ks_department_tid';
	$handler->display->display_options['filters']['ks_department_tid_1']['operator'] = 'not';
	$handler->display->display_options['filters']['ks_department_tid_1']['value'] = array(
	  0 => '12',
	);
	$handler->display->display_options['filters']['ks_department_tid_1']['vocabulary'] = 'ks_departments';

	/* Display: Events Page */
	$handler = $view->new_display('page', 'Events Page', 'page_1');
	$handler->display->display_options['defaults']['title'] = FALSE;
	$handler->display->display_options['title'] = 'Upcoming Events';
	$handler->display->display_options['defaults']['pager'] = FALSE;
	$handler->display->display_options['pager']['type'] = 'none';
	$handler->display->display_options['pager']['options']['offset'] = '0';
	$handler->display->display_options['defaults']['relationships'] = FALSE;
	/* Relationship: Content: Slider Image (ks_image:fid) */
	$handler->display->display_options['relationships']['ks_image_fid']['id'] = 'ks_image_fid';
	$handler->display->display_options['relationships']['ks_image_fid']['table'] = 'field_data_ks_image';
	$handler->display->display_options['relationships']['ks_image_fid']['field'] = 'ks_image_fid';
	$handler->display->display_options['relationships']['ks_image_fid']['label'] = 'Event Image';
	$handler->display->display_options['defaults']['fields'] = FALSE;
	/* Field: Content: Start Date */
	$handler->display->display_options['fields']['ks_start_date']['id'] = 'ks_start_date';
	$handler->display->display_options['fields']['ks_start_date']['table'] = 'field_data_ks_start_date';
	$handler->display->display_options['fields']['ks_start_date']['field'] = 'ks_start_date';
	$handler->display->display_options['fields']['ks_start_date']['label'] = '';
	$handler->display->display_options['fields']['ks_start_date']['element_label_colon'] = FALSE;
	$handler->display->display_options['fields']['ks_start_date']['element_default_classes'] = FALSE;
	$handler->display->display_options['fields']['ks_start_date']['type'] = 'date_plain';
	$handler->display->display_options['fields']['ks_start_date']['settings'] = array(
	  'format_type' => 'long',
	  'fromto' => 'both',
	  'multiple_number' => '',
	  'multiple_from' => '',
	  'multiple_to' => '',
	);
	/* Field: Content: Title */
	$handler->display->display_options['fields']['title']['id'] = 'title';
	$handler->display->display_options['fields']['title']['table'] = 'node';
	$handler->display->display_options['fields']['title']['field'] = 'title';
	$handler->display->display_options['fields']['title']['label'] = '';
	$handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
	$handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
	$handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
	$handler->display->display_options['fields']['title']['link_to_node'] = FALSE;
	/* Field: Content: Location */
	$handler->display->display_options['fields']['ks_location']['id'] = 'ks_location';
	$handler->display->display_options['fields']['ks_location']['table'] = 'field_data_ks_location';
	$handler->display->display_options['fields']['ks_location']['field'] = 'ks_location';
	$handler->display->display_options['fields']['ks_location']['label'] = '';
	$handler->display->display_options['fields']['ks_location']['element_label_colon'] = FALSE;
	/* Field: File: Path */
	$handler->display->display_options['fields']['uri']['id'] = 'uri';
	$handler->display->display_options['fields']['uri']['table'] = 'file_managed';
	$handler->display->display_options['fields']['uri']['field'] = 'uri';
	$handler->display->display_options['fields']['uri']['relationship'] = 'ks_image_fid';
	$handler->display->display_options['fields']['uri']['label'] = '';
	$handler->display->display_options['fields']['uri']['alter']['strip_tags'] = TRUE;
	$handler->display->display_options['fields']['uri']['element_label_colon'] = FALSE;
	$handler->display->display_options['fields']['uri']['element_default_classes'] = FALSE;
	$handler->display->display_options['fields']['uri']['hide_alter_empty'] = FALSE;
	$handler->display->display_options['fields']['uri']['file_download_path'] = TRUE;
	/* Field: Content: Featured Event */
	$handler->display->display_options['fields']['ks_is_featured']['id'] = 'ks_is_featured';
	$handler->display->display_options['fields']['ks_is_featured']['table'] = 'field_data_ks_is_featured';
	$handler->display->display_options['fields']['ks_is_featured']['field'] = 'ks_is_featured';
	$handler->display->display_options['fields']['ks_is_featured']['label'] = '';
	$handler->display->display_options['fields']['ks_is_featured']['element_label_colon'] = FALSE;
	$handler->display->display_options['fields']['ks_is_featured']['element_default_classes'] = FALSE;
	$handler->display->display_options['fields']['ks_is_featured']['hide_alter_empty'] = FALSE;
	/* Field: Content: Nid */
	$handler->display->display_options['fields']['nid']['id'] = 'nid';
	$handler->display->display_options['fields']['nid']['table'] = 'node';
	$handler->display->display_options['fields']['nid']['field'] = 'nid';
	$handler->display->display_options['fields']['nid']['label'] = '';
	$handler->display->display_options['fields']['nid']['element_label_colon'] = FALSE;
	$handler->display->display_options['fields']['nid']['element_default_classes'] = FALSE;
	$handler->display->display_options['defaults']['sorts'] = FALSE;
	/* Sort criterion: Content: Start Date (ks_start_date) */
	$handler->display->display_options['sorts']['ks_start_date_value']['id'] = 'ks_start_date_value';
	$handler->display->display_options['sorts']['ks_start_date_value']['table'] = 'field_data_ks_start_date';
	$handler->display->display_options['sorts']['ks_start_date_value']['field'] = 'ks_start_date_value';
	$handler->display->display_options['sorts']['ks_start_date_value']['order'] = 'DESC';
	$handler->display->display_options['defaults']['filter_groups'] = FALSE;
	$handler->display->display_options['defaults']['filters'] = FALSE;
	/* Filter criterion: Content: Published */
	$handler->display->display_options['filters']['status']['id'] = 'status';
	$handler->display->display_options['filters']['status']['table'] = 'node';
	$handler->display->display_options['filters']['status']['field'] = 'status';
	$handler->display->display_options['filters']['status']['value'] = 1;
	$handler->display->display_options['filters']['status']['group'] = 1;
	$handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
	/* Filter criterion: Content: Type */
	$handler->display->display_options['filters']['type']['id'] = 'type';
	$handler->display->display_options['filters']['type']['table'] = 'node';
	$handler->display->display_options['filters']['type']['field'] = 'type';
	$handler->display->display_options['filters']['type']['value'] = array(
	  'event' => 'event',
	);
	/* Filter criterion: Content: Department (ks_department) */
	$handler->display->display_options['filters']['ks_department_tid']['id'] = 'ks_department_tid';
	$handler->display->display_options['filters']['ks_department_tid']['table'] = 'field_data_ks_department';
	$handler->display->display_options['filters']['ks_department_tid']['field'] = 'ks_department_tid';
	$handler->display->display_options['filters']['ks_department_tid']['value'] = array(
	  7 => '7',
	  8 => '8',
	  9 => '9',
	  11 => '11',
	  10 => '10',
	);
	$handler->display->display_options['filters']['ks_department_tid']['exposed'] = TRUE;
	$handler->display->display_options['filters']['ks_department_tid']['expose']['operator_id'] = 'ks_department_tid_op';
	$handler->display->display_options['filters']['ks_department_tid']['expose']['operator'] = 'ks_department_tid_op';
	$handler->display->display_options['filters']['ks_department_tid']['expose']['identifier'] = 'ks_department_tid';
	$handler->display->display_options['filters']['ks_department_tid']['expose']['remember_roles'] = array(
	  2 => '2',
	  1 => 0,
	  3 => 0,
	);
	$handler->display->display_options['filters']['ks_department_tid']['expose']['reduce'] = TRUE;
	$handler->display->display_options['filters']['ks_department_tid']['type'] = 'select';
	$handler->display->display_options['filters']['ks_department_tid']['vocabulary'] = 'ks_departments';
	$handler->display->display_options['filters']['ks_department_tid']['error_message'] = FALSE;
	/* Filter criterion: Content: Department (ks_department) */
	$handler->display->display_options['filters']['ks_department_tid_1']['id'] = 'ks_department_tid_1';
	$handler->display->display_options['filters']['ks_department_tid_1']['table'] = 'field_data_ks_department';
	$handler->display->display_options['filters']['ks_department_tid_1']['field'] = 'ks_department_tid';
	$handler->display->display_options['filters']['ks_department_tid_1']['operator'] = 'not';
	$handler->display->display_options['filters']['ks_department_tid_1']['value'] = array(
	  12 => '12',
	);
	$handler->display->display_options['filters']['ks_department_tid_1']['type'] = 'select';
	$handler->display->display_options['filters']['ks_department_tid_1']['vocabulary'] = 'ks_departments';
	$handler->display->display_options['path'] = 'events';

	$views[$view->name] = $view;
	return $views;
}
