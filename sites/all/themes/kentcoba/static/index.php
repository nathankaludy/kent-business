<!DOCTYPE html>
<!--[if IE 8]> 				 <html class="no-js lt-ie9" lang="en" > <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en" > <!--<![endif]-->

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<title>Foundation 4</title>

	<link rel="stylesheet" href="css/normalize.css">
	<link rel="stylesheet" href="css/style.css">

	<script src="js/vendor/custom.modernizr.js"></script>
	<script src="//use.typekit.net/lnd8wnu.js"></script>
	<script>try{Typekit.load();}catch(e){}</script>
</head>
<body>




	<a href="#" style="display: block;width: 10px; height: 10px; background-color: red; position: fixed; z-index: 99999; top: 0; left: 0;" onclick="$('.guides').toggleClass('visible'); return false;"></a>
	<div class="row guides">
		<div class="large-1 medium-1 small-1 columns"></div>
		<div class="large-1 medium-1 small-1 columns"></div>
		<div class="large-1 medium-1 small-1 columns"></div>
		<div class="large-1 medium-1 small-1 columns"></div>
		<div class="large-1 medium-1 small-1 columns"></div>
		<div class="large-1 medium-1 small-1 columns"></div>
		<div class="large-1 medium-1 small-1 columns"></div>
		<div class="large-1 medium-1 small-1 columns"></div>
		<div class="large-1 medium-1 small-1 columns"></div>
		<div class="large-1 medium-1 small-1 columns"></div>
		<div class="large-1 medium-1 small-1 columns"></div>
		<div class="large-1 medium-1 small-1 columns"></div>
		<div class="large-1 medium-1 small-1 columns"></div>
		<div class="large-1 medium-1 small-1 columns"></div>
		<div class="large-1 medium-1 small-1 columns"></div>
		<div class="large-1 medium-1 small-1 columns"></div>
	</div>

	<header></header>



	<h1 class="title-main">College of Business Administration</h1>

	<div class="top contain-to-grid">
		<nav class="top-bar">
			<ul class="title-area">
				<li class="name">College of Business Administration</li>
				<li class="toggle-topbar menu-icon"><a href="#"><span>Menu</span></a></li>
			</ul>

			<div class="top-search">
				<a href="#" class="button-search"><i class="i-magnifier">&nbsp;</i></a>
				<div id="block-search-form" class="block block-search" data-thmr="thmr_55">
					<form action="#" method="post" id="search-block-form" accept-charset="UTF-8" data-thmr="thmr_54">
						<input title="Enter the terms you wish to search for." type="text" id="edit-search-block-form--2" name="search_block_form" placeholder="Search" size="15" maxlength="128" class="form-text" data-thmr="thmr_46">
						<input type="submit" id="edit-submit" name="op" value="Search" class="form-submit" data-thmr="thmr_52">
						<input type="hidden" name="form_build_id" value="form-k23W6JDOXGBrOv-0tUL0jqCs3SjBLz9n2iQWtQkbi4Y" data-thmr="thmr_49">
						<input type="hidden" name="form_token" value="YnSBPg2LtodP6GglxLnPsCaq2ZhbtKIEGdP2id8nlQo" data-thmr="thmr_50">
						<input type="hidden" name="form_id" value="search_block_form" data-thmr="thmr_51">
					</form>
				</div>
			</div>
			<section class="top-bar-section">
				<ul>
					<li><a href="#"><span>About Us</span></a></li>
					<li><a href="#"><span>Future Students</span></a></li>
					<li><a href="#"><span>Current Students</span></a></li>
					<li><a href="#"><span>Degrees</span></a></li>
					<li><a href="#"><span>Global</span></a></li>
					<li><a href="#"><span>Careers &amp; Internships</span></a></li>
					<li class="has-dropdown"><a href="#"><span>Faculty &amp; Staff</span></a>
						<ul class="dropdown">
							<li class="has-dropdown">
								<a href="#">Dropdown Level 1a</a>
								<ul class="dropdown">
									<li><a href="#">Dropdown Level 2a</a></li>
									<li><a href="#">Dropdown Level 2b</a></li>
									<li class="has-dropdown"><a href="#">Dropdown Level 2c</a>
										<ul class="dropdown">
											<li><a href="#">Dropdown Level 3a</a></li>
											<li><a href="#">Dropdown Level 3b</a></li>
											<li><a href="#">Dropdown Level 3c</a></li>
										</ul>
									</li>
									<li><a href="#">Dropdown Level 2d</a></li>
									<li><a href="#">Dropdown Level 2e</a></li>
									<li><a href="#">Dropdown Level 2f</a></li>
								</ul>
							</li>
							<li><a href="#">Dropdown Level 1b</a></li>
							<li><a href="#">Dropdown Level 1c</a></li>
							<li><a href="#">Dropdown Level 1d</a></li>
							<li><a href="#">Dropdown Level 1e</a></li>
						</ul>
					</li>
				</ul>
			</section>
		</nav>
	</div>


	<!-- Homepage -->
	<div class="layout-homepage">

		<!-- Main Slider -->
		<div class="flexslider flexslider-home-main">
			<span class="label">From Kent, the world.</span>
			<ul class="slides">
				<li style="background-image: url(img/tmp_homepage1.jpg);">
					<h4><a href="#">Lorem Ipsum dolor sit amer, consectetur adipisicing.</a></h4>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do...</p>
					<a href="#" class="button-slide">READ MORE</a>
				</li>
				<li style="background-image: url(img/tmp_homepage2.jpg);">
					<h4><a href="#">Euismod Mollis Lorem Ornare</a></h4>
					<p>Donec ullamcorper nulla non metus auctor fringilla. Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio...</p>
					<a href="#" class="button-slide">READ MORE</a>
				</li>
				<li style="background-image: url(img/tmp_homepage3.jpg);">
					<h4><a href="#">Aenean lacinia bibendum nulla sed consectetur.</a></h4>
					<p>Sed posuere consectetur est at lobortis....</p>
					<a href="#" class="button-slide">READ MORE</a>
				</li>
				<li style="background-image: url(img/tmp_homepage4.jpg);">
					<h4><a href="#">Lorem Ipsum dolor sit amer, consectetur adipisicing.</a></h4>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do...</p>
					<a href="#" class="button-slide">READ MORE</a>
				</li>
			</ul>
		</div>
		<!-- /Main Slider -->


		<!-- News Slider -->
		<div class="row collapse">
			<div class="large-12 medium-16 large-centered columns" style="z-index:80">

				<ul class="news-slider">
					<li class="news-slider-nav">
						<h1 class="dotted-after dotted-color6 color6 h-center">News</h1>

						<div class="news-slider-nav-controls">
							<a href="#" class="i-arrow-left-white-light">Previous Page</a>
							<div><strong>4</strong> / <span>4</span></div>
							<a href="#" class="i-arrow-right-white-light">Next Page</a>
						</div>

						<a href="#" class="button tiny round color2">Archive</a>
					</li>
					<?php $i=0; while($i<5): ?>
					<li>
						<article class="news-summary-slider">
							<div class="news-photo" style="background-image: url(img/tmp_news_thumb.jpg)"></div>
							<div class="news-overlay"></div>
							<div class="news-content">
								<h2 class="dotted-after dotted-color6 h-center"><a href="#">Kent State Econ Professor Gives 2014 Predictions For your Wallet.</a></h2>
								<span class="news-domain">wallethub.com</span>
							</div>
							<a href="#" class="button round color4 b-readmore">Read&nbsp;More</a>
						</article>
					</li>
					<?php $i++; endwhile; ?>
				</ul>

			</div>
		</div>
		<!-- /News Slider -->


		<!-- Video -->
		<article class="video-full">
			<div>
				<h4>College Spotlight</h4>
				<p>Lorem ipsum dolor sit amet, consectetur adipielit, sed do eiusmod temporsicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
				<a href="#" class="button round">Play Video <i class="i-play-white"></i></a>
				<div class="video-photo" style="background-image: url(img/tmp_homepage2.jpg);"></div>
			</div>
		</article>
		<!-- /Video -->


		<div class="panel panel-small even-3">
			<div class="row">
				<a href="#" class="button round">Apply Now</a>
				<a href="#" class="button round">Schedule A Visit</a>
				<a href="#" class="button round">Find a Major</a>
			</div>
		</div>


		<!-- Events -->
		<div class="panel-pattern">
			<div class="row collapse events-intro-home">

					<div class="events-filters">
						<div class="wrap">
							<h1 class="subheader dotted-after dotted-color6 color6 h-center">Events</h1>
							<a href="#" data-dropdown="drop" class="button dropdown round color2">All Events</a>
							<div class="button round split-action color2">
								<a href="#"><span class="i-arrow-left-white">&lt;</span></a>
								December 02 – 08
								<a href="#"><span class="i-arrow-right-white">&gt;</span></a>
							</div>
						</div>
					</div>


					<div class="flexslider-home-events-container flexslider-controls-color2">
						<div class="flexslider flexslider-home-events">
							<ul class="slides">
								<?php $x=0; while($x<3): ?>
								<li>
									<?php $i=0; while($i<6): ?>
									<article class="event-summary" itemscope itemtype="http://schema.org/Event">
										<time itemprop="startDate" content="2014-04-08T06:30">
											Wed	<span class="dd">04</span> Dec
										</time>
										<div class="event-content">
											<a itemprop="url" href="url" class="color5">
												<span class="dotted-after dotted-color5" itemprop="name">Executive MBA for Healthcare Info Session</span>
											</a>
											<span class="time">6:30 p.m.</span>
											<span class="location" itemprop="location" itemscope>Twinsburg RAC</span>
										</div>
									</article>
									<?php $i++; endwhile; ?>
								</li>
								<?php $x++; endwhile; ?>
							</ul>
						</div>
						<a href="#" class="button round color1">See more Events</a>
					</div>

					<div class="flexslider-home-events-container-small">
						<h1 class="subheader color6 h-center dotted-after dotted-color6">Events</h1>
						<div class="flexslider flexslider-home-events-small">
							<ul class="slides">
								<?php $x=0; while($x<5): ?>
								<li>
									<article class="event-summary-white" itemscope itemtype="http://schema.org/Event">
										<time itemprop="startDate" content="2014-04-08T06:30">
											Wed	<span class="dd">04</span> Dec
										</time>
										<div class="event-content">
											<a itemprop="url" href="url" class="color6">
												<span class="dotted-after dotted-color6" itemprop="name">Executive MBA for Healthcare Info Session</span>
											</a>
											<span class="time">6:30 p.m.</span>
											<span class="location" itemprop="location" itemscope>Twinsburg RAC</span>
										</div>
									</article>
								</li>
								<?php $x++; endwhile; ?>
							</ul>
						</div>
						<a href="#" class="button-text color5 right">See all Events</a>
					</div>

			</div>
		</div>
		<!-- /Events -->


		<!-- Secondary Slider -->
		<div class="flexslider flexslider-home-secondary">
			<ul class="slides">
				<li>
					<article>
						<div>
							<h4>Study Abroad</h4>
							<p>Lorem ipsum dolor sit amet, consectetur adipielit, sed do eiusmod temporsicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
							<a href="#" class="button round color5">Learn More</a>
							<div class="slide-photo" style="background-image: url(img/tmp_homepage1.jpg);"></div>
						</div>
					</article>
				</li>
				<li>
					<article>
						<div>
							<h4>Euismod Mollis Lorem Ornare</h4>
							<p>Lorem ipsum dolor sit amet, consectetur adipielit, sed do eiusmod temporsicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
							<a href="#" class="button round color5">Learn More</a>
							<div class="slide-photo" style="background-image: url(img/tmp_homepage2.jpg);"></div>
						</div>
					</article>
				</li>
				<li>
					<article>
						<div>
							<h4>Aenean laci nulla sed consectetur.</h4>
							<p>Lorem ipsum dolor sit amet, consectetur adipielit, sed do eiusmod temporsicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
							<a href="#" class="button round color5">Learn More</a>
							<div class="slide-photo" style="background-image: url(img/tmp_homepage3.jpg);"></div>
						</div>
					</article>
				</li>
				<li>
					<article>
						<div>
							<h4>Lorem Ipsum dolor sit.</h4>
							<p>Lorem ipsum dolor sit amet, consectetur adipielit, sed do eiusmod temporsicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
							<a href="#" class="button round color5">Learn More</a>
							<div class="slide-photo" style="background-image: url(img/tmp_homepage4.jpg);"></div>
						</div>
					</article>
				</li>
			</ul>
		</div>
		<!-- /Secondary Slider -->


		<div class="panel-color2 panel-small panel-center">
			<div class="row">
				<a href="#" class="button small round color6">Apply Now</a>
				<a href="#" class="button small round color6">Schedule Advising</a>
			</div>
		</div>


	</div>
	<!-- /Homepage -->



	<!-- Main Layout -->
	<div class="layout-main">
		<div class="sidebar">

			<h2 class="subheader dotted-after dotted-color5">Degrees</h2>
			<ul class="side-nav">
				<li><a href="#">Undergraduate Majors</a></li>
				<li><a href="#">Undergraduate Minors</a></li>
				<li class="active"><a href="#">Certificates</a></li>
				<li><a href="#">Donec ullamcorper nulla non metus auctor fringilla. Master’s Programs</a></li>
				<li><a href="#">Executive MBA Programs</a></li>
				<li><a href="#">Ph.D.</a></li>
			</ul>

		</div>
		<div class="content">

			<h1>Headline</h1>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip.</p>
			<p>Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Vestibulum id ligula porta felis euismod semper. Nullam id dolor id nibh ultricies vehicula ut id elit. Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Donec ullamcorper nulla non metus auctor fringilla.</p>

			<hr>

			<p class="text-intro">Ut enim ad minim veniam, quis nostrud exercitation ullamco <span class="color2">Why Accounting?</span> Laboris nisi ut aliquip ex ea commodo consequat.</p>

			<div class="panel panel-color0 panel-small even-2">
				<div class="row">
					<a href="#" class="button round small color1">Apply Now</a>
					<a href="#" class="button round small color1">Schedule Advising</a>
				</div>
			</div>


			<h2>Headline 2</h2>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip.</p>

			<article class="block-summary">
				<h3><a href="#">Undergraduate Majors</a></h3>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo.</p>
				<a href="#" class="button-text color2">View Majors</a>
			</article>
			<article class="block-summary">
				<h3><a href="#">Undergraduate Majors</a></h3>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo.</p>
				<a href="#" class="button-text color2">View Majors</a>
			</article>
			<article class="block-summary">
				<h3><a href="#">Undergraduate Majors</a></h3>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo.</p>
				<a href="#" class="button-text color2">View Majors</a>
			</article>
			<article class="block-summary">
				<h3><a href="#">Undergraduate Majors</a></h3>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo.</p>
				<a href="#" class="button-text color2">View Majors</a>
			</article>

			<hr>

			<h2>Marketing</h2>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip.</p>

			<article class="block-summary">
				<h3><a href="#">Undergraduate Majors</a></h3>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo.</p>
				<a href="#" class="button-text color2">View Majors</a>
			</article>
			<article class="block-summary">
				<h3><a href="#">Undergraduate Majors</a></h3>
				<a href="#" class="photo"><img src="img/tmp_news_full.jpg" alt=""></a>
			</article>
			<article class="block-summary">
				<h3><a href="#">Undergraduate Majors</a></h3>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo.</p>
				<a href="#" class="button-text color2">View Majors</a>
			</article>
			<article class="block-summary">
				<h3><a href="#">Undergraduate Majors</a></h3>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo.</p>
				<a href="#" class="button-text color2">View Majors</a>
			</article>

			<br>

			<div class="panel panel-photo-left">
				<div class="panel-photo" style="background-image: url(img/tmp_panel.jpg)"></div>
				<div class="panel-content">
					<h2>Scholarships</h2>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
					<a href="#" class="button round color6">Learn More</a>
				</div>
			</div>

			<div class="panel panel-color2">
				<h2>Internships</h2>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
				<a href="#" class="button round color6">Learn More</a>
			</div>


			<div class="panel-pattern">
				<h2>Student Organizations</h2>

				<div class="row gutter">
					<div class="large-8 medium-8 small-16 columns">
						<article class="block-summary">
							<h3><a href="#">Undergraduate Majors</a></h3>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo.</p>
							<a href="#" class="button-text color2">View Majors</a>
						</article>
					</div>
					<div class="large-8 medium-8 small-16 columns">
						<article class="block-summary">
							<h3><a href="#">Undergraduate Majors</a></h3>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo.</p>
							<a href="#" class="button-text color2">View Majors</a>
						</article>
					</div>
				</div>
			</div>


			<!--
			<div class="panel panel-color0">
				<h2>Internships</h2>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
				<a href="#" class="button round color6">Learn More</a>
			</div>
			<div class="panel panel-color1">
				<h2>Internships</h2>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
				<a href="#" class="button round color6">Learn More</a>
			</div>
			<div class="panel panel-color2">
				<h2>Internships</h2>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
				<a href="#" class="button round color6">Learn More</a>
			</div>
			<div class="panel panel-color3">
				<h2>Internships</h2>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
				<a href="#" class="button round color6">Learn More</a>
			</div>
			<div class="panel panel-color4">
				<h2>Internships</h2>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
				<a href="#" class="button round color6">Learn More</a>
			</div>
			-->

			<h2 class="h2-5" id="here">Faculty</h2>

			<article class="profile-full-summary">
				<img src="img/tmp_profile.jpg" alt="" class="photo active">

				<div class="profile-content">
					<h2>C. Lockwood Reynolds, CFP, CPA</h2>
					<strong class="profile-job-title">Assistant Professor<br>Department of Economics</strong><br>
					(330) 672-2545<br>
					<a href="mailto:" class="button-text-email">email@kent.edu</a>

					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>

					<a href="#" class="button round">Faculty Directory</a>
				</div>
			</article>


			<h2>Research</h2>

			<div class="list-papers">
				<article class="paper-summary">
					<p><strong class="paper-title">Reynolds, C. Lockwood, and Shawn Rohlin, 2014.</strong> <a href="#">“Do Location-based Tax Incentives Improve Quality of Life and Quality of Business Environment.”</a> Journal of Regional Science, 54(1), pp.1-32. [final version or DOI: 10.1111/jors.12035/abstract]</p>
				</article>
				<article class="paper-summary">
					<p><strong class="paper-title">Reynolds, C. Lockwood, and Shawn Rohlin, 2014.</strong> <a href="#">“Do Location-based Tax Incentives Improve Quality of Life and Quality of Business Environment.”</a> Journal of Regional Science, 54(1), pp.1-32. [final version or DOI: 10.1111/jors.12035/abstract]</p>
				</article>
				<article class="paper-summary">
					<p><strong class="paper-title">Reynolds, C. Lockwood, and Shawn Rohlin, 2014.</strong> <a href="#">“Do Location-based Tax Incentives Improve Quality of Life and Quality of Business Environment.”</a> Journal of Regional Science, 54(1), pp.1-32. [final version or DOI: 10.1111/jors.12035/abstract]</p>
				</article>
				<article class="paper-summary">
					<p><strong class="paper-title">Reynolds, C. Lockwood, and Shawn Rohlin, 2014.</strong> <a href="#">“Do Location-based Tax Incentives Improve Quality of Life and Quality of Business Environment.”</a> Journal of Regional Science, 54(1), pp.1-32. [final version or DOI: 10.1111/jors.12035/abstract]</p>
				</article>
			</div>


			<hr>


			<h2>Advisory Board</h2>

			<article class="block-full-summary">
				<a href="#" class="photo"><img src="img/tmp_news_full.jpg"></a>

				<div class="block-content">
					<h3><a href="#">Undergraduate Majors</a></h3>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo.</p>
				</div>
				<a href="#" class="button round">Button</a>
			</article>


			<div class="panel-pattern">
				<h2 class="h2-5">Contact</h2>

				<div class="list-contacts">
					<article>
						<h4 class="subheader color5">Office</h4>
						<p>(330) 672-2545<br>Fax: (330) 672-2548<br>550 Business Admistration Bldg<br><a href="#" class="button-text color2">Email</a></p>
					</article>
					<article>
						<h4 class="subheader color5">Office</h4>
						<p>(330) 672-2545<br>
						Fax: (330) 672-2548<br>
						550 Business Admistration Bldg<br>
						<a href="#" class="button-text color2">Email</a></p>
					</article>
					<article>
						<h4 class="subheader color5">Office</h4>
						<p>(330) 672-2545<br>
						Fax: (330) 672-2548<br>
						550 Business Admistration Bldg<br>
						<a href="#" class="button-text color2">Email</a></p>
					</article>
					<article>
						<h4 class="subheader color5">Office</h4>
						<p>(330) 672-2545<br>
						Fax: (330) 672-2548<br>
						550 Business Admistration Bldg<br>
						<a href="#" class="button-text color2">Email</a></p>
					</article>
				</div>
			</div>

		</div>
	</div>
	<!-- /Main Layout -->





	<br><br><br>




	<!-- Forms -->
	<div class="layout-main">
		<div class="sidebar">
			<h2 class="subheader dotted-after dotted-color5">Forms</h2>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
			<a href="#" class="button tiny round color0">Link</a>
		</div>

		<div class="content">

			<h1>Headline</h1>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>


			<form class="custom">
				<fieldset>
				<legend>Fieldset</legend>

					<div class="row">
						<div class="small-16 columns">
						<!-- <label>Input Label</label> -->
						<input type="text" placeholder="small-16.columns">
						</div>
					</div>

					<div class="row">
						<div class="small-8 columns">
							<!-- <label>Input Label</label> -->
							<input type="text" placeholder="small-8.columns">
						</div>
						<div class="small-8 columns">
							<div class="row collapse">
								<!-- <label>Input Label</label> -->
								<div class="small-12 columns">
									<input type="text" placeholder="small-12.columns">
								</div>
								<div class="small-4 columns">
									<span class="postfix">.com</span>
								</div>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="small-16 columns">
						<!-- <label>Textarea Label</label> -->
							<textarea placeholder="small-16.columns"></textarea>
						</div>
					</div>

					<div class="row">
						<div class="small-16 columns">
							<select id="customDropdown1" class="small-16 columns">
							<option DISABLED>This is a dropdown</option>
							<option>This is another option</option>
							<option>This is another option too</option>
							<option>Look, a third option</option>
							</select>
						</div>
					</div>

				</fieldset>

				<fieldset>
				<legend>Fieldset</legend>

					<div class="row">
						<div class="small-8 columns">
							<label class="radio-label" for="radio1"><input name="radio1" type="radio" id="radio1" style="display:none;" CHECKED><span class="custom radio checked"></span> Radio Button 1</label>
							<label class="radio-label" for="radio1"><input name="radio1" type="radio" id="radio1" style="display:none;"><span class="custom radio"></span> Radio Button 2</label>
							<label class="radio-label" for="radio1"><input name="radio1" type="radio" id="radio1" disabled style="display:none;"><span class="custom radio"></span> Radio Button 3</label>
						</div>
						<div class="small-8 columns">
							<label class="checkbox-label" for="checkbox1"><input type="checkbox" id="checkbox1" style="display: none;"><span class="custom checkbox"></span> Label for Checkbox</label>
							<label class="checkbox-label" for="checkbox2"><input type="checkbox" id="checkbox2" CHECKED style="display: none;"><span class="custom checkbox checked"></span> Label for Checkbox</label>
							<label class="checkbox-label" for="checkbox3"><input type="checkbox" CHECKED id="checkbox3" style="display: none;"><span class="custom checkbox checked"></span> Label for Checkbox</label>
						</div>
					</div>
				</fieldset>


				<fieldset>
				<legend>Fieldset</legend>

					<div class="row">
						<div class="small-8 columns">
							<div class="row collapse">
								<div class="small-9 columns">
									<span class="prefix">mysubdomain</span>
								</div>
								<div class="small-7 columns">
									<select>
									<option>.com</option>
									<option>.co</option>
									<option>.ca</option>
									</select>
								</div>
							</div>
						</div>
						<div class="small-8 columns">
							<div class="row collapse">
								<div class="small-12 columns">
								<select>
									<option>google</option>
									<option>yahoo</option>
									<option>bing</option>
								</select>
								</div>
								<div class="small-4 columns">
									<span class="postfix">.com</span>
								</div>
							</div>
						</div>
					</div>

				</fieldset>


				<div class="text-right">
					<input type="submit" class="button round">
				</div>

			</form>
		</div>
	</div>
	<!-- /Forms -->



	<br><br><br>

	<!-- News Details -->
	<div class="layout-content-with-author">
		<div class="sidebar">
			<h2 class="subheader dotted-after dotted-color5">News</h2>
			<a href="#" class="button-icon color2"><i class="i-arrow-left-color2"></i>Back</a>
		</div>

		<div class="content">
			<article class="news">
				<img src="img/tmp_news_full.jpg" class="large photo active" alt="">

				<div class="content-body">
					<time class="round">07/04/2014</time>

					<span class="title-prefix color2">Category</span>
					<h1 class="dotted-after dotted-color2 color8">Kent State Econ Professor Gives 2014 Predictions For your Wallet.</h1>

					<p>Cras mattis consectetur purus sit amet fermentum. Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Donec ullamcorper nulla non metus auctor fringilla. Cras justo odio, dapibus ac facilisis in, egestas eget quam.</p>
					<p>Maecenas faucibus mollis interdum. Nullam quis risus eget urna mollis ornare vel eu leo. Curabitur blandit tempus porttitor. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla vitae elit libero, a pharetra augue. Nullam quis risus eget urna mollis ornare vel eu leo. Praesent commodo cursus magna, vel scelerisque nisl consectetur et.</p>
					<p>Nulla vitae elit libero, a pharetra augue. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Sed posuere consectetur est at lobortis. Nullam quis risus eget urna mollis ornare vel eu leo. Maecenas faucibus mollis interdum.</p>

					<p><strong>Pellentesque habitant morbi tristique</strong> senectus et netus et <abbr title="HyperText Markup Language">HTML</abbr> fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. <em>Aenean ultricies mi vitae est.</em> Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, <code>commodo vitae</code>, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui. <a href="#">Donec non enim</a> in turpis pulvinar facilisis. Ut felis.</p>
					<p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Vestibulum id ligula porta felis euismod semper. Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Aenean lacinia bibendum nulla sed consectetur. Nulla vitae elit libero, a pharetra augue.</p>
					<ul>
						<li>Lorem ipsum nulla aute elit in aliqua sit consequat sint in qui consectetur et consectetur nulla occaecat.</li>
						<li>Lorem ipsum elit minim in Excepteur labore in qui amet eiusmod irure. </li>
						<li>Lorem ipsum dolore veniam exercitation consectetur amet consectetur nostrud ullamco irure non adipisicing. </li>
						<li>Lorem ipsum ex cillum in Ut aute in pariatur. Lorem ipsum ad ut non culpa commodo velit cillum consectetur Ut do in eiusmod ut officia.
							<ol>
								<li>Lorem ipsum in culpa tempor non laborum ut sed deserunt mollit Ut reprehenderit in magna.
									<ul>
										<li>Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</li>
										<li>Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</li>
										<li>Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</li>
									</ul>
								</li>
								<li>Lorem ipsum tempor in ea Ut cillum sint pariatur ex fugiat. Lorem ipsum consectetur in cillum officia ut ut Excepteur ut occaecat minim anim anim sunt commodo minim ex. Lorem ipsum Ut in exercitation nisi id ullamco cillum. Lorem ipsum dolor dolor labore eu nostrud ut.
									<ol>
										<li>Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</li>
										<li>Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</li>
										<li>Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</li>
									</ol>
								</li>
							</ol>
						</li>
						<li>Lorem ipsum nulla voluptate occaecat dolor aliquip tempor adipisicing laborum sit voluptate dolor ex elit enim. Lorem ipsum dolore aliquip ut exercitation elit Ut cillum do in esse. </li>
					</ul>
					<p>Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam quis risus eget urna mollis ornare vel eu leo. Nullam quis risus eget urna mollis ornare vel eu leo.</p>
					<p>Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Nulla vitae elit libero, a pharetra augue. Sed posuere consectetur est at lobortis. Donec id elit non mi porta gravida at eget metus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam quis risus eget urna mollis ornare vel eu leo. Nulla vitae elit libero, a pharetra augue.</p>
					<h2>How to Apply for Full Club Status</h2>
					<p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Maecenas faucibus mollis interdum. Cras mattis consectetur purus sit amet fermentum. Maecenas faucibus mollis interdum. Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Maecenas sed diam eget risus varius blandit sit amet non magna.</p>
					<p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Maecenas faucibus mollis interdum. Cras mattis consectetur purus sit amet fermentum. Maecenas faucibus mollis interdum. Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Maecenas sed diam eget risus varius blandit sit amet non magna.</p>
					<blockquote>
						Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus magna. Cras in mi at felis aliquet congue. Ut a est eget ligula molestie gravida. Curabitur massa. Donec eleifend, libero at sagittis mollis, tellus est malesuada tellus, at luctus turpis elit sit amet quam. Vivamus pretium ornare est.
						<cite>Unknow Source</cite>
					</blockquote>
					<p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Maecenas faucibus mollis interdum. Cras mattis consectetur purus sit amet fermentum. Maecenas faucibus mollis interdum. Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Maecenas sed diam eget risus varius blandit sit amet non magna.</p>


<blockquote class="large-quote">
	<div class="quote-content">
		<p>“The College of Business Administration Career Services Offices got me in contact with the internship course professor and helped me fill out the necessary paperwork in order to get school credit. Ic teculpa quibus, sunt omnihitatios issitas alique sunto evenis earumentio. Itassedi diati ulliqui dolorep rovidel igendit omnisciende cullandit ea ped et ut dolupta cupta dollandis eum sit lab im lacipic illitist omnistrumditius.”</p>
	</div>

	<div class="quote-author">
		<img src="http://kent-coba.plankdesign.com/sites/default/files/Spake_Headshot_About.jpg" class="photo">
		<h3>Kate Nigro</h3>
		<p>Senior Managerial Marketing and Computer Information Systems Major</p>
	</div>
</blockquote>


					<table style="width: 100%;" border="0">
					<thead>
					<tr>
					<th>Ornare</th>
					<th>Vestibulum</th>
					<th>Ullamcorper</th>
					<th>Egestas</th>
					</tr>
					</thead>
					<tbody>
					<tr>
					<td>Mollis</td>
					<td>Ipsum</td>
					<td>Cras</td>
					<td>Vulputate</td>
					</tr>
					<tr>
					<td>Ultricies</td>
					<td>Vehicula</td>
					<td>Dapibus</td>
					<td>Elit</td>
					</tr>
					<tr>
					<td>Adipiscing</td>
					<td>Porta</td>
					<td>Magna</td>
					<td>Ligula</td>
					</tr>
					<tr>
					<td>Aenean</td>
					<td>Ligula</td>
					<td>Amet</td>
					<td>Ullamcorper</td>
					</tr>
					</tbody>
					</table>
					<p>Etiam porta sem malesuada magna mollis euismod. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Maecenas faucibus mollis interdum. Nulla vitae elit libero, a pharetra augue.</p>
					<p>Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Maecenas sed diam eget risus varius blandit sit amet non magna. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Donec id elit non mi porta gravida at eget metus. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
					<hr>
					<p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Maecenas faucibus mollis interdum. Cras mattis consectetur purus sit amet fermentum. Maecenas faucibus mollis interdum. Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Maecenas sed diam eget risus varius blandit sit amet non magna.</p>
					<p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Maecenas faucibus mollis interdum. Cras mattis consectetur purus sit amet fermentum. Maecenas faucibus mollis interdum. Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Maecenas sed diam eget risus varius blandit sit amet non magna.</p>
					<table style="width: 100%;" border="0" class="table-style1">
					<thead>
					<tr>
					<th>Ornare</th>
					<th>Vestibulum</th>
					<th>Ullamcorper</th>
					<th>Egestas</th>
					</tr>
					</thead>
					<tbody>
					<tr>
					<td>Mollis</td>
					<td>Ipsum</td>
					<td>Cras</td>
					<td>Vulputate</td>
					</tr>
					<tr>
					<td>Ultricies</td>
					<td>Vehicula</td>
					<td>Dapibus</td>
					<td>Elit</td>
					</tr>
					<tr>
					<td>Adipiscing</td>
					<td>Porta</td>
					<td>Magna</td>
					<td>Ligula</td>
					</tr>
					<tr>
					<td>Aenean</td>
					<td>Ligula</td>
					<td>Amet</td>
					<td>Ullamcorper</td>
					</tr>
					</tbody>
					</table>


				</div>
				<div class="content-author">

					<h2 class="subheader dotted-after dotted-color2">Featured Expert</h2>

					<article class="profile-summary small">
						<a href="#" class="photo"><img src="img/tmp_profile.jpg" alt=""></a>
						<h2><a href="#">First Last</a></h2>
						<strong class="profile-job-title">Title</strong><br>
						(330) 672-2545<br>
						<a href="mailto:" class="button-text-email">email@kent.edu</a>
					</article>

				</div>
			</article>
		</div>
	</div>
	<!-- /News Details -->


	<br><br><br>




	<!-- News Listing -->
	<div class="layout-main">
		<div class="sidebar">
			<h2 class="subheader dotted-after dotted-color5">News</h2>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
			<a href="#" class="button tiny round color0">Link</a>
		</div>

		<div class="content">

			<div class="title-dropdown">
				<h1>Sort By</h1>
				<a href="#" data-dropdown="drop" class="button dropdown round color2">Filter</a>
				<a href="#" data-dropdown="drop" class="button dropdown round color2">Filter</a>
				<a href="#" data-dropdown="drop" class="button dropdown round color2">Year</a>
			</div>

			<article class="news-summary">
				<img src="img/tmp_news_thumb.jpg" alt="">
				<div class="news-overlay"></div>
				<div class="news-content">
					<time class="round">07/04/2014</time>
					<h2 class="dotted-after dotted-color5 h-center"><a href="">Kent State Econ Professor Gives 2014 Predictions For your Wallet.</a></h2>
					<span class="news-domain">wallethub.com</span>
				</div>
				<a href="#" class="button round color4 b-readmore">Read More</a>
			</article>
			<br>
			<article class="news-summary">
				<div class="news-overlay"></div>
				<div class="news-content">
					<time class="round">07/04/2014</time>
					<h2 class="dotted-after dotted-color5 h-center"><a href="">Kent State Econ Professor Gives 2014 Predictions For your Wallet.</a></h2>
					<span class="news-domain">wallethub.com</span>
				</div>
				<a href="#" class="button round color4 b-readmore">Read More</a>
			</article>
			<br>
			<article class="news-summary">
				<img src="img/tmp_news_thumb.jpg" alt="">
				<div class="news-overlay"></div>
				<div class="news-content">
					<time class="round">07/04/2014</time>
					<h2 class="dotted-after dotted-color5 h-center"><a href="">Cras mattis consectetur purus sit amet fermentum. Curabitur blandit tempus porttitor. Etiam porta sem malesuada magna mollis euismod. Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Praesent commodo cursus magna, vel scelerisque nisl consectetur et.</a></h2>
					<span class="news-domain">wallethub.com</span>
				</div>
				<a href="#" class="button round color4 b-readmore">Read More</a>
			</article>

			<a href="" class="button large round color0 expand">Load More</a>
		</div>
	</div>
	<!-- /News Listing -->


	<br><br><br>






	<!-- Profile Details -->
	<div class="layout-main">
		<div class="sidebar">
			<h2 class="subheader dotted-after dotted-color5">Faculty &amp; Staff</h2>
			<a href="#" class="button-icon color2"><i class="i-arrow-left-color2"></i>Back</a>
		</div>

		<div class="content">

			<article class="profile">
				<img src="img/tmp_profile.jpg" alt="" class="photo active">

				<div class="intro">
					<h1>C. Lockwood Reynolds, CFP, CPA</h1>
					<strong class="profile-job-title">Assistant Professor<br>Department of Economics</strong><br>
					(330) 672-2545<br>
					<a href="mailto:" class="button-text-email">email@kent.edu</a>
				</div>

				<section class="first">
					<h3>Education</h3>
					<p>Ph.D., Economics, University of Michigan, 2007<br>M.A., Economics, University of Michigan, 2004<br>B.A., Economics, Williams College, 2001<br>Ph.D., Economics, University of Michigan, 2007<br>M.A., Economics, University of Michigan, 2004<br>B.A., Economics, Williams College, 2001<br>Ph.D., Economics, University of Michigan, 2007<br>M.A., Economics, University of Michigan, 2004<br>B.A., Economics, Williams College, 2001</p>
					<p>Ph.D., Economics, University of Michigan, 2007<br>M.A., Economics, University of Michigan, 2004<br>B.A., Economics, Williams College, 2001<br>Ph.D., Economics, University of Michigan, 2007<br>M.A., Economics, University of Michigan, 2004<br>B.A., Economics, Williams College, 2001<br>Ph.D., Economics, University of Michigan, 2007<br>M.A., Economics, University of Michigan, 2004<br>B.A., Economics, Williams College, 2001</p>
				</section>

				<section>
					<h3>Biography</h3>
					<p>C. Lockwood Reynolds received his Ph.D. from the University of Michigan in 2007. His teaching and research interests are in microeconomics, labor economics, public economics and the economics of education. He primarily researches access and completion in higher education with a particular focus on the effects of college choice and the role of various household resources on educational decisions. He also researches the local effects of government policies designed to encourage economic activity with a focus on the spatial or distributional implications of such policies.</p>
					<p>Dr. Reynolds teaches Principles of Microeconomics and the Economics of Labor Markets. He also teaches Managerial Economics in the MBA program and is a three-time winner of the Outstanding MBA Professor Award.</p>
					<a href="#" class="button-text color2">Curriculum Vitae</a>
				</section>

				<section>
					<h3>Areas Of Expertise</h3>
					<p>Labor Economics<br>Public Economics<br>Economics of Education<br>Urban Economics</p>
				</section>

				<section>
					<h3>Selected Publications</h3>

					<div class="list-papers">
						<article class="paper-summary">
							<p><strong class="paper-title">Reynolds, C. Lockwood, and Shawn Rohlin, 2014.</strong> <a href="#">“Do Location-based Tax Incentives Improve Quality of Life and Quality of Business Environment.”</a> Journal of Regional Science, 54(1), pp.1-32. [final version or DOI: 10.1111/jors.12035/abstract]</p>
						</article>
						<article class="paper-summary">
							<p><strong class="paper-title">Reynolds, C. Lockwood, and Shawn Rohlin, 2014.</strong> <a href="#">“Do Location-based Tax Incentives Improve Quality of Life and Quality of Business Environment.”</a> Journal of Regional Science, 54(1), pp.1-32. [final version or DOI: 10.1111/jors.12035/abstract]</p>
						</article>
						<article class="paper-summary">
							<p><strong class="paper-title">Reynolds, C. Lockwood, and Shawn Rohlin, 2014.</strong> <a href="#">“Do Location-based Tax Incentives Improve Quality of Life and Quality of Business Environment.”</a> Journal of Regional Science, 54(1), pp.1-32. [final version or DOI: 10.1111/jors.12035/abstract]</p>
						</article>
						<article class="paper-summary">
							<p><strong class="paper-title">Reynolds, C. Lockwood, and Shawn Rohlin, 2014.</strong> <a href="#">“Do Location-based Tax Incentives Improve Quality of Life and Quality of Business Environment.”</a> Journal of Regional Science, 54(1), pp.1-32. [final version or DOI: 10.1111/jors.12035/abstract]</p>
						</article>
					</div>
				</section>

				<section>
					<h3>Working Papers And Research</h3>

					<div class="list-papers">
						<article class="paper-summary">
							<p><strong class="paper-title">Reynolds, C. Lockwood, and Shawn Rohlin, 2014.</strong> <a href="#">“Do Location-based Tax Incentives Improve Quality of Life and Quality of Business Environment.”</a> Journal of Regional Science, 54(1), pp.1-32. [final version or DOI: 10.1111/jors.12035/abstract]</p>
						</article>
						<article class="paper-summary">
							<p><strong class="paper-title">Reynolds, C. Lockwood, and Shawn Rohlin, 2014.</strong> <a href="#">“Do Location-based Tax Incentives Improve Quality of Life and Quality of Business Environment.”</a> Journal of Regional Science, 54(1), pp.1-32. [final version or DOI: 10.1111/jors.12035/abstract]</p>
						</article>
						<article class="paper-summary">
							<p><strong class="paper-title">Reynolds, C. Lockwood, and Shawn Rohlin, 2014.</strong> <a href="#">“Do Location-based Tax Incentives Improve Quality of Life and Quality of Business Environment.”</a> Journal of Regional Science, 54(1), pp.1-32. [final version or DOI: 10.1111/jors.12035/abstract]</p>
						</article>
					</div>
				</section>
			</article>
		</div>
	</div>
	<!-- /Profile Details -->


	<br><br><br>


	<!-- Profile Listing -->
	<div class="layout-main">
		<div class="sidebar">
			<h2 class="subheader dotted-after dotted-color5">Faculty &amp; Staff</h2>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
			<a href="#" class="button tiny round color0">Link</a>
		</div>

		<div class="content">

			<div class="title-dropdown">
				<h1>Sort By</h1>
				<a href="#" data-dropdown="drop" class="button dropdown round color2">Filter</a>
				<a href="#" data-dropdown="drop" class="button dropdown round color2">Filter</a>
			</div>

			<?php $az=0; while($az<2): ?>
			<div class="list-profiles">
				<a href="#A" name="A" class="letter">A</a>

				<?php $i=0; while($i<8): ?>
				<article class="profile-summary">
					<a href="#" class="photo"><img src="img/tmp_profile.jpg" alt=""></a>
					<h2><a href="#">C. Lockwood Reynolds, CFP, CPA</a></h2>
					<strong class="profile-job-title">Title</strong><br>
					(330) 672-2545<br>
					<a href="mailto:" class="button-text-email">email@kent.edu</a>
				</article>
				<?php $i++; endwhile; ?>
			</div>
			<?php $az++; endwhile; ?>

			<a href="" class="button large round color0 expand">Load More</a>

		</div>
	</div>
	<!-- /Profile Listing -->


	<br><br><br>




	<div class="row collapse events-intro">
		<!-- Event Filters -->
		<div class="events-filters">
			<div class="wrap">
				<h1 class="subheader dotted-after dotted-color6 color6 h-center">Events</h1>
				<a href="#" data-dropdown="drop" class="button dropdown round color2">All Events</a>
				<div class="button round split-action color2">
					<a href="#"><span class="i-arrow-left-white">&lt;</span></a>
					December 02 – 08
					<a href="#"><span class="i-arrow-right-white">&gt;</span></a>
				</div>
			</div>
		</div>
		<!-- /Event Filters -->

		<!-- Featured Event -->
		<div class="event-featured" itemscope itemtype="http://schema.org/Event" style="background-image:url(img/tmp_event_featured.jpg)">
			<article>
				<time itemprop="startDate" content="2014-04-08T06:30">
					Wed	<span class="dd">04</span> Dec
				</time>
				<div class="event-content">
					<a itemprop="url" href="url" class="color6">
						<span class="dotted-after dotted-color6" itemprop="name">Executive MBA for Healthcare Info Session</span>
					</a>
					<span class="time">6:30 p.m.</span>
					<span class="location" itemprop="location" itemscope>Twinsburg RAC</span>
				</div>
			</article>
		</div>
		<!-- /Featured Event -->
	</div>


	<div class="list-events-3col">
		<?php $i=0; while($i<=13): ?>
		<!-- Event -->
		<article class="event-summary" itemscope itemtype="http://schema.org/Event">
			<time itemprop="startDate" content="2014-04-08T06:30">
				Wed	<span class="dd">04</span> Dec
			</time>
			<div class="event-content">
				<a itemprop="url" href="url" class="color5">
					<span class="dotted-after dotted-color5" itemprop="name">Executive MBA for Healthcare Info Session</span>
				</a>
				<span class="time">6:30 p.m.</span>
				<span class="location" itemprop="location" itemscope>Twinsburg RAC</span>
			</div>
		</article>
		<!-- /Event -->
		<?php $i++; endwhile; ?>
	</div>





<br><br><br><br>


	<div class="row collapse">
		<div class="large-14 large-centered columns">
			<h1>H1 Headline --- Museo Slab, 500 – 32px/32px – #002664 Integer posuere erat a ante venenatis posuere velit aliquet.</h1>
			<h2>Kitchen Sink</h2>

			<p>Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Nullam id dolor id nibh ultricies vehicula ut id elit. Sed posuere consectetur est at lobortis.</p>
			<p>Donec sed odio dui. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Cras mattis consectetur purus sit amet fermentum. Praesent commodo cursus magna, vel scelerisque nisl consectetur et.</p>
			<p>Curabitur blandit <a href="#">tempus porttitor. Donec sed odio dui.</a> Curabitur blandit tempus porttitor. Integer posuere erat a ante venenatis dapibus posuere velit aliquet.</p>
			<p>Sed posuere consectetur est at lobortis. Nullam quis risus eget urna mollis ornare vel eu leo. Curabitur blandit tempus porttitor. Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
			<p>Nullam id dolor id nibh ultricies vehicula ut id elit. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Donec id elit non mi porta gravida at eget metus.</p>


			<h4>Alert Boxes</h4>

			<div data-alert class="alert-box radius">
				This is a standard alert (div.alert-box).
				<a href="" class="close">&times;</a>
			</div>

			<div data-alert class="alert-box success">
				This is a success alert (div.alert-box.success).
				<a href="" class="close">&times;</a>
			</div>

			<div data-alert class="alert-box alert round">
				This is an alert (div.alert-box.alert.round).
				<a href="" class="close">&times;</a>
			</div>

			<div data-alert class="alert-box secondary">
				This is a secondary alert (div.alert-box.secondary).
				<a href="" class="close">&times;</a>
			</div>

			<hr>

			<h4>Block Grid</h4>

			<ul class="small-block-grid-2 large-block-grid-4">
				<li><img src="http://foundation.zurb.com/docs/v/4.3.2/img/demos/demo1.jpg"></li>
				<li><img src="http://foundation.zurb.com/docs/v/4.3.2/img/demos/demo2.jpg"></li>
				<li><img src="http://foundation.zurb.com/docs/v/4.3.2/img/demos/demo3.jpg"></li>
				<li><img src="http://foundation.zurb.com/docs/v/4.3.2/img/demos/demo4.jpg"></li>
			</ul>

			<hr>

			<h4>Breadcrumbs</h4>

			<ul class="breadcrumbs">
				<li><a href="#">Home</a></li>
				<li><a href="#">Features</a></li>
				<li class="unavailable"><a href="#">Gene Splicing</a></li>
				<li class="current"><a href="#">Cloning</a></li>
			</ul>

			<hr>

			<h4>Buttons</h4>

			<a href="#" class="tiny button">.tiny.button</a><br><br>
			<a href="#" class="small button">.small.button</a><br><br>
			<a href="#" class="button">.button</a><br><br>
			<a href="#" class="large button">.large.button</a><br><br>

			<a href="#" class="tiny button round">.tiny.button.round</a><br><br>
			<a href="#" class="small button round">.small.button.round</a><br><br>
			<a href="#" class="button round">.button.round</a><br><br>
			<a href="#" class="large button round">.large.button.round</a><br><br>

			<a href="#" class="button round secondary">.button.round.secondary</a><br><br>
			<a href="#" class="button round color0">.button.round.color0</a><br><br>
			<a href="#" class="button round color1">.button.round.color1</a><br><br>
			<a href="#" class="button round color2">.button.round.color2</a><br><br>
			<a href="#" class="button round color3">.button.round.color3</a><br><br>
			<a href="#" class="button round color4">.button.round.color4</a><br><br>
			<a href="#" class="button round color6">.button.round.color6</a>


			<hr>

			<h4>Button Groups</h4>

			<ul class="button-group">
				<li><a href="#" class="small button">Button 1 Small</a></li>
				<li><a href="#" class="small button">Button 2 Small</a></li>
				<li><a href="#" class="small button">Button 3 Small</a></li>
			</ul>
			<ul class="button-group radius">
				<li><a href="#" class="button secondary">Button 1</a></li>
				<li><a href="#" class="button secondary">Button 2</a></li>
				<li><a href="#" class="button secondary">Button 3</a></li>
				<li><a href="#" class="button secondary">Button 4</a></li>
			</ul>
			<ul class="button-group round even-3">
				<li><a href="#" class="button alert">Button 1</a></li>
				<li><a href="#" class="button alert">Button 2</a></li>
				<li><a href="#" class="button alert">Button 3</a></li>
			</ul>
			<ul class="button-group round even-3">
				<li><input type="submit" class="button success" value="Button 1"></li>
				<li><input type="submit" class="button success" value="Button 2"></li>
				<li><input type="submit" class="button success" value="Button 3"></li>
			</ul>

			<hr>

			<h4>Dropdown Buttons</h4>
			<ul id="drop" class="f-dropdown content round" data-dropdown-content>
				<li><a href="#">This is a link</a></li>
				<li><a href="#">This is another</a></li>
				<li><a href="#">Yet another</a></li>
			</ul>

			<a href="#" data-dropdown="drop" class="tiny button dropdown color2">Dropdown Button</a><br><br>
			<a href="#" data-dropdown="drop" class="small button dropdown color2">Dropdown Button</a><br><br>
			<a href="#" data-dropdown="drop" class="button dropdown color2">Dropdown Button</a><br><br>
			<a href="#" data-dropdown="drop" class="large button dropdown color2">Dropdown Button</a><br><br>

			<a href="#" data-dropdown="drop" class="tiny round button dropdown color2">Dropdown Button</a><br><br>
			<a href="#" data-dropdown="drop" class="small round button dropdown color2">Dropdown Button</a><br><br>
			<a href="#" data-dropdown="drop" class="round button dropdown color2">Dropdown Button</a><br><br>
			<a href="#" data-dropdown="drop" class="large round button dropdown color2">Dropdown Button</a>

			<hr>

			<h4>Clearing</h4>

			<div>
				<ul class="clearing-thumbs" data-clearing>
				<li><a class="th" href="http://foundation.zurb.com/docs/v/4.3.2/img/demos/demo1.jpg"><img data-caption="Nulla vitae elit libero, a pharetra augue. Cras mattis consectetur purus sit amet fermentum." src="http://foundation.zurb.com/docs/v/4.3.2/img/demos/demo1-th.jpg"></a></li>
				<li><a class="th" href="http://foundation.zurb.com/docs/v/4.3.2/img/demos/demo2.jpg"><img src="http://foundation.zurb.com/docs/v/4.3.2/img/demos/demo2-th.jpg"></a></li>
				<li><a class="th" href="http://foundation.zurb.com/docs/v/4.3.2/img/demos/demo3.jpg"><img data-caption="Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Cras mattis consectetur purus sit amet fermentum. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus." src="http://foundation.zurb.com/docs/v/4.3.2/img/demos/demo3-th.jpg"></a></li>
				<li><a class="th" href="http://foundation.zurb.com/docs/v/4.3.2/img/demos/demo4.jpg"><img src="http://foundation.zurb.com/docs/v/4.3.2/img/demos/demo4-th.jpg"></a></li>
				<li><a class="th" href="http://foundation.zurb.com/docs/v/4.3.2/img/demos/demo5.jpg"><img data-caption="Integer posuere erat a ante venenatis dapibus posuere velit aliquet." src="http://foundation.zurb.com/docs/v/4.3.2/img/demos/demo5-th.jpg"></a></li>
				</ul>
			</div>

			<hr>

			<h4>Flex Video</h4>

			<div class="flex-video">
				<iframe width="420" height="315" src="http://www.youtube.com/embed/0_EW8aNgKlA" frameborder="0" allowfullscreen></iframe>
			</div>

			<hr>

			<h4>Inline Lists</h4>

			<ul class="inline-list">
				<li><a href="#">Link 1</a></li>
				<li><a href="#">Link 2</a></li>
				<li><a href="#">Link 3</a></li>
				<li><a href="#">Link 4</a></li>
				<li><a href="#">Link 5</a></li>
			</ul>

			<hr>

			<h4>Labels</h4>

			<p>
				<span class="label">Regular Label</span><br>
				<span class="radius secondary label">Radius Secondary Label</span><br>
				<span class="round alert label">Round Alert Label</span><br>
				<span class="success label">Success Label</span><br>
			</p>

			<hr>

			<h4>Pagination</h4>

			<ul class="pagination">
				<li class="arrow unavailable"><a href="">&laquo;</a></li>
				<li class="current"><a href="">1</a></li>
				<li><a href="">2</a></li>
				<li><a href="">3</a></li>
				<li><a href="">4</a></li>
				<li class="unavailable"><a href="">&hellip;</a></li>
				<li><a href="">12</a></li>
				<li><a href="">13</a></li>
				<li class="arrow"><a href="">&raquo;</a></li>
			</ul>

			<hr>

			<h4>Panels</h4>

			<div class="row">
				<div class="large-8 columns">
				<div class="panel">
					<h5>This is a regular panel.</h5>
					<p>It has an easy to override visual style, and is appropriately subdued.</p>
				</div>
				</div>
				<div class="large-8 columns">
				<div class="panel callout radius">
					<h5>This is a callout panel.</h5>
					<p>It's a little ostentatious, but useful for important content.</p>
				</div>
				</div>
			</div>

			<hr>

			<h4>Reveal</h4>

			<a href="#" data-reveal-id="firstModal" class="radius button">Example Modal&hellip;</a>
			<a href="#" data-reveal-id="videoModal" class="radius button">Example Modal w/Video&hellip;</a>

			<hr>

			<h4>Side Nav</h4>

			<div class="row">
				<div class="large-4 columns end">
				<ul class="side-nav">
					<li class="active"><a href="#">Link 1</a></li>
					<li><a href="#">Vestibulum id ligula porta</a></li>
					<li><a href="#">Felis euismod semper</a></li>
					<li><a href="#">Sem Mattis Inceptos</a></li>
					<li class="divider"></li>
					<li><a href="#">Lattis Nibh Sit</a></li>
					<li><a href="#">LMattis Nibh Sitink 4</a></li>
				</ul>
				</div>
			</div>

			<hr>

			<h4>Sub Nav</h4>

			<dl class="sub-nav">
				<dt>Filter:</dt>
				<dd class="active"><a href="#">All</a></dd>
				<dd><a href="#">Active</a></dd>
				<dd><a href="#">Pending</a></dd>
				<dd><a href="#">Suspended</a></dd>
			</dl>

			<hr>

			<h4>Tables</h4>

			<table>
				<thead>
				<tr>
					<th width="200">Table Header</th>
					<th>Table Header</th>
					<th width="150">Table Header</th>
					<th width="150">Table Header</th>
				</tr>
				</thead>
				<tbody>
				<tr>
					<td>Content Goes Here</td>
					<td>This is longer content Donec id elit non mi porta gravida at eget metus.</td>
					<td>Content Goes Here</td>
					<td>Content Goes Here</td>
				</tr>
				<tr>
					<td>Content Goes Here</td>
					<td>This is longer Content Goes Here Donec id elit non mi porta gravida at eget metus.</td>
					<td>Content Goes Here</td>
					<td>Content Goes Here</td>
				</tr>
				<tr>
					<td>Content Goes Here</td>
					<td>This is longer Content Goes Here Donec id elit non mi porta gravida at eget metus.</td>
					<td>Content Goes Here</td>
					<td>Content Goes Here</td>
				</tr>
				</tbody>
			</table>

			<hr>

			<h4>Thumbnails</h4>

			<p><a class="th" href="http://foundation.zurb.com/docs/v/4.3.2/img/demos/demo1.jpg"><img src="http://foundation.zurb.com/docs/v/4.3.2/img/demos/demo1-th.jpg"></a></p>
			<img class="th" src="http://foundation.zurb.com/docs/v/4.3.2/img/demos/demo2-th.jpg">

			<hr>

			<h4>Tooltips</h4>

			<p>The tooltips can be positioned on the <span data-tooltip class="has-tip" data-width="210" title="I'm on bottom and the default position.">"tip-bottom"</span>, which is the default position, <span data-tooltip class="has-tip tip-top" data-width="210" title="I'm on the top!">"tip-top" (hehe)</span>, <span data-tooltip="left" class="has-tip tip-left" data-width="90" title="I'm on the left!">"tip-left"</span>, or <span data-tooltip="right" class="has-tip tip-right" data-width="120" title="I'm on the right!">"tip-right"</span> of the target element by adding the appropriate class to them. You can even add your own custom class to style each tip differently. On a small device, the tooltips are full width and bottom aligned.</p>

			<hr>

			<h4>Type</h4>

			<h1>h1. This is a very large header.</h1>
			<h2>h2. This is a large header.</h2>
			<h3>h3. This is a medium header.</h3>
			<h4>h4. This is a moderate header.</h4>
			<h5>h5. This is a small header. h1.</h5>
			<h6>h6. This is a tiny header. h1.</h6>

			<br>

			<h1 class="dotted-before dotted-color5">h1. This is a very large header.</h1>
			<h2 class="dotted-before dotted-color5">h2. This is a large header.</h2>
			<h3 class="dotted-before dotted-color5">h3. This is a medium header.</h3>
			<h4 class="dotted-before dotted-color5">h4. This is a moderate header.</h4>
			<h5 class="dotted-before dotted-color5">h5. This is a small header. h1.</h5>
			<h6 class="dotted-before dotted-color5">h6. This is a tiny header. h1.</h6>

			<br>

			<h1 class="subheader">h1.subheader</h1>
			<h2 class="subheader">h2.subheader</h2>
			<h3 class="subheader">h3.subheader</h3>
			<h4 class="subheader">h4.subheader</h4>
			<h5 class="subheader">h5.subheader</h5>
			<h6 class="subheader">h6.subheader</h6>


			<br>

			<h1 class="subheader dotted-before dotted-color5">h1.subheader.dotted-before</h1>
			<h2 class="subheader dotted-before dotted-color5">h2.subheader.dotted-before</h2>
			<h3 class="subheader dotted-before dotted-color5">h3.subheader.dotted-before</h3>
			<h4 class="subheader dotted-before dotted-color5">h4.subheader.dotted-before</h4>
			<h5 class="subheader dotted-before dotted-color5">h5.subheader.dotted-before</h5>
			<h6 class="subheader dotted-before dotted-color5">h6.subheader.dotted-before</h6>

			<hr>

			<dl>
				<dt>Definition List</dt>
				<dd>Definition Cras justo odio, dapibus ac facilisis in, egestas eget quam. Nullam id dolor id nibh ultricies vehicula ut id elit.</dd>
				<dt>Definition List</dt>
				<dd>Sed posuere consectetur est at lobortis. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Cras mattis consectetur purus sit amet fermentum. Nullam quis risus eget urna mollis ornare vel eu leo. Etiam porta sem malesuada magna mollis euismod.</dd>
				<dt>Definition List</dt>
				<dd>Definition Cras justo odio, dapibus ac facilisis in, egestas eget quam. Nullam id dolor id nibh ultricies vehicula ut id elit.</dd>
				<dt>Definition List</dt>
				<dd>Definition Cras justo odio, dapibus ac facilisis in, egestas eget quam. Nullam id dolor id nibh ultricies vehicula ut id elit.</dd>

			</dl>

			<br>

			<ul class="vcard">
				<li class="fn">Gaius Baltar</li>
				<li class="street-address">123 Colonial Ave.</li>
				<li class="locality">Caprica City</li>
				<li><span class="state">Caprica</span>, <span class="zip">12345</span></li>
				<li class="email"><a href="#">g.baltar@cmail.com</a></li>
			</ul>

		</div>
	</div>


	<!-- Panel Small-->
	<div class="panel-color0 panel-small panel-center">
		<a href="#" class="button small round color1">Apply Now</a>
		<a href="#" class="button small round color1">Schedule Advising</a>
	</div>

	<div class="panel panel-small panel-center">
		<a href="#" class="button small round">Apply Now</a>
		<a href="#" class="button small round">Schedule Advising</a>
	</div>
	<div class="panel-color2 panel-small panel-center">
		<a href="#" class="button small round color6">Apply Now</a>
		<a href="#" class="button small round color6">Schedule Advising</a>
	</div>
	<div class="panel-color3 panel-small panel-center">
		<a href="#" class="button small round color6">Apply Now</a>
		<a href="#" class="button small round color6">Schedule Advising</a>
	</div>
	<div class="panel-color4 panel-small panel-center">
		<a href="#" class="button small round color6">Apply Now</a>
		<a href="#" class="button small round color6">Schedule Advising</a>
	</div>
	<div class="panel-color5 panel-small panel-center">
		<a href="#" class="button small round color6">Apply Now</a>
		<a href="#" class="button small round color6">Schedule Advising</a>
	</div>
	<div class="panel-color6 panel-small panel-center">
		<a href="#" class="button small round color2">Apply Now</a>
		<a href="#" class="button small round color2">Schedule Advising</a>
	</div>
	<div class="panel-color7 panel-small panel-center">
		<a href="#" class="button small round color6">Apply Now</a>
		<a href="#" class="button small round color6">Schedule Advising</a>
	</div>
	<!-- /Panel Small-->


	<!-- COBA Footer -->
	<footer>
		<div class="row collapse">
			<div class="large-4 large-push-1 medium-4 medium-push-1 columns">

				<address>
					<h4 class="subheader dotted-before dotted-color7 color6">The College of business Administration</h4>
					(330) 672-3000<br>
					P.O. Box 5190<br>
					Kent, OH <br>
					44242-0001 USA
				</address>

				<address>
					<h4 class="subheader dotted-before dotted-color7 color6">Dean’s office</h4>
					(330) 672-2772<br>
					<a href="#" class="button-text color2">Dean@kent.edu</a>
				</address>

				<ul class="footer-social">
					<li><a href="#" target="_blank"><i class="fi-social-facebook"></i></a></li>
					<li><a href="#" target="_blank"><i class="fi-social-twitter"></i></a></li>
					<li><a href="#" target="_blank"><i class="fi-social-instagram"></i></a></li>
				</ul>

				<div class="footer-all-accounts">
					<h4 class="subheader">Kent State University</h4>
					<p><a href="#" class="i-more-white"></a> All University Accounts</p>
				</div>
			</div>
			<div class="large-9 large-pull-1 medium-9 medium-pull-1 columns footer-contact">

				<address>
					<h4 class="subheader dotted-before dotted-color7 color6">Management &amp; Information Systems Department</h4>
					(330) 672-2426<br>
					<a href="#" class="button-text color2">finance@kent.edu</a>
				</address>

				<?php $i=0; while($i<4): ?>
				<address>
					<h4 class="subheader dotted-before dotted-color7 color6">Accounting Department</h4>
					(330) 672-2545<br>
					<a href="#" class="button-text color2">Accounting@kent.edu</a>
				</address>

				<address>
					<h4 class="subheader dotted-before dotted-color7 color6">Economics Department</h4>
					(330) 672-2366<br>
					<a href="#" class="button-text color2">Economics@Kent.edu</a>
				</address>
				<?php $i++; endwhile; ?>

			</div>
		</div>


		<div class="row collapse footer-logos">
			<h4 class="subheader color6">Earned Excellence</h4>
			<a href="#"><img src="img/logos/aacsb.png" alt=""></a>
			<a href="#"><img src="img/logos/usnews.png" alt=""></a>
			<a href="#"><img src="img/logos/princetonreview.png" alt=""></a>
			<a href="#"><img src="img/logos/ceomagazine.png" alt=""></a>
		</div>

	</footer>
	<!-- /COBA Footer -->



	<script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
	<script src="js/jquery.flexslider.js"></script>
	<script src="js/foundation.min.js"></script>


	<script>
	$(document).foundation();
	$(function(){
		$('.flexslider-home-main').flexslider({
			directionNav: false
		});
		$('.flexslider-home-secondary').flexslider({
			directionNav: false
		});
		$('.flexslider-home-events').flexslider({
			animation: "slide",
			controlsContainer: ".flexslider-home-events-container",
			directionNav: false
		});
		$('.flexslider-home-events-small').flexslider({
			animation: "slide",
			controlsContainer: ".flexslider-home-events-container-small",
			controlNav: false,
			directionNav: true
		});


		// Search Toggle
		$(".button-search").on("click", function(e){
			e.preventDefault();
			$(this).toggleClass("active");
			$(this).siblings(".block-search").toggleClass("active");
		});
	});
	</script>

	<!--[if LT IE 9]>
	<script>
		$(".columns:last-child").addClass("last-child");
		$(".footer-contact address:nth-child(3n + 1)").addClass("nth-child-3n1");
		$(".footer-contact address:nth-last-child(-n + 3)").addClass("nth-last-child-n3");
		$(".even-2, .even-3, .even-4, .even-5, .even-6, .even-7, .even-8, .even-9, .even-10").find("*:last-child").addClass("last-child");
		$(".list-profiles .profile-summary:nth-child(4n+1)").addClass("nth-child-4n1");
		$(".list-profiles .profile-summary:nth-child(4n+2)").addClass("nth-child-4n2");
	</script>
	<![endif]-->
</body>
</html>
