<?php if ($variables['main_menu']): ?>
<h1 class="title-main"><?= l($variables['site_name']); ?></h1>

<div class="top contain-to-grid">
	<nav class="top-bar">
		<ul class="title-area">
			<li class="name"><?= l($variables['site_name']); ?></li>
			<li class="toggle-topbar menu-icon"><a href="#"><span>Menu</span></a></li>
		</ul>

		<div class="top-search">
			<a href="#" class="button-search"><i class="i-magnifier">&nbsp;</i></a>
			<div id="block-search-form" class="block block-search">
				<form action="/search/node" method="post" id="search-block-form" accept-charset="UTF-8">
					<input title="Enter the terms you wish to search for." type="text" id="keyword-search" name="search_block_form" placeholder="Search" size="15" maxlength="128" class="form-text">
					<input type="submit" id="edit-submit" name="op" value="Search" class="form-submit">
				</form>
			</div>
		</div>

		<section class="top-bar-section">
			<?php print theme('links__system_main_menu', array('links' => $variables['main_menu'], 'attributes' => array('id' => 'main-menu'))); ?>
		</section>
	</nav>
</div>
<?php endif; ?>
