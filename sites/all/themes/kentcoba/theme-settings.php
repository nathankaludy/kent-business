<?php

function kentcoba_form_system_theme_settings_alter(&$form, $form_state) {
	$form['kentcoba'] = array(
		'#type' => 'fieldset',
		'#title' => 'Kent State',
		'#description' => 'Settings specific to Kent State COBA theme',
		'newsletter_heading' => array(
			'#type' => 'textfield',
			'#title' => 'Newsletter Page Heading',
			'#description' => 'Top-most heading that will appear on newsletter page',
			'#default_value' => theme_get_setting('newsletter_heading'),
		),
		'newsletter_blurb' => array(
			'#type' => 'textarea',
			'#title' => 'Newsletter Page Blurb',
			'#description' => 'This short snippet of text will appear beneath the newsletter heading',
			'#default_value' => theme_get_setting('newsletter_blurb'),
		),
		'newsletter_button_text' => array(
			'#type' => 'textfield',
			'#title' => 'Newsletter Subscribe Button Text',
			'#description' => 'Text for "Subscribe to our Newsletter" button',
			'#default_value' => theme_get_setting('newsletter_button_text'),
		),
		'newsletter_button_target' => array(
			'#type' => 'textfield',
			'#title' => 'Newsletter Subscribe Button Target',
			'#description' => 'URL to which the subscribe button will link',
			'#default_value' => theme_get_setting('newsletter_button_target'),
		),
	);
}
