<?php $link = base_path() . $view->field['field_advisory_board_link']->last_render_text; ?>

<h2 class="h2-5">Advisory Board</h2>
<article class="block-full-summary">
	<?php if (!empty($view->field['field_image']->last_render_text)): ?>
	<a href="<?= $link; ?>" class="photo">
		<?= $view->field['field_image']->last_render_text; ?>
	</a>
	<?php endif; ?>
	<div class="block-content">
		<?= $view->field['field_advisory_board_copy']->last_render_text; ?>
	</div>
	<a href="<?= $link; ?>" class="button round">Read More</a>
</article>
