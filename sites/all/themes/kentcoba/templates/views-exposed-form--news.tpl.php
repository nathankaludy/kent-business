<?php if (!empty($q)): ?>
	<?php
		// This ensures that, if clean URLs are off, the 'q' is added first so that
		// it shows up first in the URL.
		print $q;
	?>
<?php endif; ?>

<?php foreach ($widgets as $widget): ?>
	<?= $widget->widget; ?>
<?php endforeach; ?>
