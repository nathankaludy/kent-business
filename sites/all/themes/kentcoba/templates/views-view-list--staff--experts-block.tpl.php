<div class="list-profiles">
	<h2 class="h2-5"><?= $view->display[$view->current_display]->display_options['block_description']; ?></h2>
	<?php foreach ($rows as $row): ?>
		<article class="profile-summary">
			<?php print $row; ?>
		</article>
	<?php endforeach; ?>
</div>
