<?php

if (isset($view->exposed_input['monday'])) {
	$monday = new DateTime($view->exposed_input['monday']);
	$sunday = clone $monday;
	$sunday->modify('+6 days');
} else {
	$monday = new DateTime('Monday this week');
	$sunday = new DateTime('Sunday this week');
}

?>

<div class="<?= $classes; ?>">
	<div class="row collapse events-intro">
		<!-- Event Filters -->
		<div class="events-filters" data-view-type="page_1">
			<div class="wrap">
				<h1 class="subheader dotted-after dotted-color6 color6 h-center">Events</h1>
				<a href="#" id="selected-department" data-dropdown="events-department-dropdown" class="button dropdown round color2">All Events</a>
				<?= $exposed; ?>

				<div id="event-date-filter" class="button round split-action color2" data-monday="<?= $monday->format('U'); ?>" data-domid="<?= $dom_id; ?>" data-view-display="page_1">
					<a data-dir="back" href="#"><span class="i-arrow-left-white">&lt;</span></a>
					<?= $monday->format('M j'); ?> - <?= $sunday->format('M j'); ?>
					<a data-dir="forward" href="#"><span class="i-arrow-right-white">&gt;</span></a>
				</div>
			</div>
		</div>
		<!-- /Event Filters -->

		<?php if ($view->useful): ?>
			<?php foreach ($view->useful as $result): ?>
				<?php if ($result['featured'] == 1): ?>
					<!-- Featured Event -->
					<?php $dt = $result['start_date']; ?>
					<?php $url = url('node/' . $result['nid']); ?>
					<div class="event-featured" itemscope itemtype="http://schema.org/Event" style="background-image:url(<?= $result['image']; ?>)" onclick="location.href='<?= $url; ?>';">
						<article>
							<time itemprop="startDate" content="<?= $dt->format('U'); ?>">
								<?= $dt->format('D'); ?> <span class="dd"><?= $dt->format('d'); ?></span> <?= $dt->format('M'); ?>
							</time>
							<div class="event-content">
								<a itemprop="url" href="<?= $url; ?>" class="color6">
									<span class="dotted-after dotted-color6" itemprop="name"><?= $result['title']; ?></span>
								</a>
								<span class="time"><?= $dt->format('h:i a'); ?></span>
								<span class="location" itemprop="location" itemscope><?= $result['location']; ?></span>
							</div>
						</article>
					</div>
					<!-- /Featured Event -->
					<?php break; ?>
				<?php endif; ?>
			<?php endforeach; ?>
		<?php else: ?>
		<p>Sorry, no results found.</p>
		<?php endif; ?>
	</div>

	<div class="list-events-3col">
		<?php foreach ($view->useful as $row): ?>
			<?php $url = url('node/' . $row['nid']); $dt = $row['start_date']; ?>
			<article class="event-summary" itemscope itemtype="http://schema.org/Event" onclick="location.href='<?= $url ?>'">
				<time itemprop="startDate" content="<?= $dt->format('U'); ?>">
					<?= $dt->format('D'); ?> <span class="dd"><?= $dt->format('d'); ?></span> <?= $dt->format('M'); ?>
				</time>
				<div class="event-content">
					<a itemprop="url" href="<?= $url; ?>" class="color5">
						<span class="dotted-after dotted-color5" itemprop="name"><?= $row['title']; ?></span>
					</a>
					<span class="time"><?= $dt->format('h:i a'); ?></span>
					<span class="location" itemprop="location" itemscope=""><?= $row['location']; ?></span>
				</div>
			</article>
		<?php endforeach; ?>
	</div>
</div>
