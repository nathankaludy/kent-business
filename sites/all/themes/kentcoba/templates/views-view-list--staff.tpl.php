<div class="list-profiles">
	<?php if (!empty($title)) : ?>
	<a href="#<?php print $title; ?>" name="<?php print $title; ?>" class="letter"><?php print $title; ?></a>
	<?php endif; ?>

	<?php foreach ($rows as $row): ?>
	<article class="profile-summary">
		<?php print $row; ?>
	</article>
	<?php endforeach; ?>
</div>
