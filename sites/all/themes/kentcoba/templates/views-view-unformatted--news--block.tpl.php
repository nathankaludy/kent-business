<div class="row collapse">
	<div class="news-slider-wrapper large-12 medium-16 large-centered columns" style="z-index:80">

		<ul class="news-slider" data-items="newsPages">
			<li class="news-slider-nav">
				<h1 class="dotted-after dotted-color6 color6 h-center">News</h1>
				<div class="news-slider-nav-controls">
					<a href="#" class="js-role-previous-page i-arrow-left-white-light">Previous Page</a>
					<div><strong class="news-slider-current">1</strong> <em>of</em> <span class="news-slider-total"><?= ceil(count($rows) / 5); ?></span></div>
					<a href="#" class="js-role-next-page i-arrow-right-white-light">Next Page</a>
				</div>
				<?= l('Archive', 'news', array('attributes' => array('class' => array('button', 'tiny', 'round', 'color2')))); ?>
			</li>

			<?php for ($i = 0; $i < 5; $i++): ?>
				<?= $rows[$i]; ?>
			<?php endfor; ?>
		</ul>

		<script>
			var newsPages = <?= $view->news_json; ?>;
		</script>
	</div>
</div>
<script id="tmplNews" class="jstemplate" type="text/template"><article class="new news-summary-slider">
		<div class="news-photo" style="background-image: url('{{img_url}}')"></div>
		<div class="news-overlay"></div>
		<div class="news-content">
			<time class="round">{{date}}</time>
			<h2 class="dotted-after dotted-color6 h-center"><a href="{{link}}" target="{{link_target}}">{{title}}</a></h2>
			<span class="news-domain">{{display_link}}</span>
		</div>
		<a href="{{link}}" class="button round color4 b-readmore" target="{{link_target}}">Read More</a>
	</article>
</script>
