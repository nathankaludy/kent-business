<div class="list-profiles">
	<?php if (!empty($title)) : ?>
		<h2><a name="<?= strtolower($title); ?>"><?= $title; ?></a></h2>
	<?php endif; ?>

	<?php foreach ($rows as $row): ?>
		<article class="profile-summary">
			<?php print $row; ?>
		</article>
	<?php endforeach; ?>
</div>
