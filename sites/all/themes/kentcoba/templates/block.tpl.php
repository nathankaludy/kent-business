<?php /*
<div id="<?php print $block_html_id; ?>" class="<?php print $classes; ?>"<?php print $attributes; ?>>
	<?php print render($title_prefix); ?>
	<?php if ($block->subject): ?>
	<h2<?php print $title_attributes; ?>><?php print $block->subject ?></h2>
	<?php endif;?>
	<?php print render($title_suffix); ?>

	<?php print $content ?>
</div>
*/ ?>
<div>
	<?php if ($block->subject): ?>
		<h2 class="subheader dotted-after dotted-color5"><?= $block->subject; ?></h2>
	<?php endif; ?>
	<?= $content; ?>
</div>