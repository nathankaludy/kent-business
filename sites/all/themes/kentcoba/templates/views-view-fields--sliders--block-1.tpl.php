<li>
	<article>
		<div>
			<h4><?= $fields['ks_heading']->content; ?></h4>
			<p><?= $fields['ks_subheading']->content; ?></p>
			<a href="<?= $fields['ks_link']->content; ?>" class="button round color5">Learn More</a>
			<div class="slide-photo" style="background-image: url(<?= $fields['uri']->content; ?>);"></div>
		</div>
	</article>
</li>
