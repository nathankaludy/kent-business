<div class="flexslider flexslider-home-main">
	<span class="label">From Kent, the world.</span>
	<ul class="slides">
		<?php foreach ($rows as $row): ?>
			<?= $row; ?>
		<?php endforeach; ?>
	</ul>
</div>