<?php $relative_path = slugify($fields['name']->content); ?>
<article class="block-summary">
	<a name="<?= $relative_path; ?>"></a>
	<h3><?= $fields['name']->content; ?></h3>
	<?= $fields['description']->content; ?>
	<a href="<?= $fields['field_link_target']->content; ?>" class="button-text color2">
		<?= $fields['field_link_text']->content; ?>
	</a>
</article>
