<?php

$name = $view->field['name']->last_render_text;
$link = $view->field['field_pdf_link']->last_render_text;
$button = $view->field['field_pdf_button_text']->last_render_text;
$upload = $view->field['field_pdf_upload']->last_render_text;

?>
<hr>
<div name="why_<?= slugify(strtolower($name)); ?>">
	<h2 class="color2">Why <?= $name; ?>?</h2>
	<p class="text-intro"><?= $view->field['field_why']->last_render_text; ?></p>
	<?php if ($upload && $button): ?>
		<a href="<?= $upload; ?>" class="button round"><?= $button; ?></a>
	<?php elseif ($link && $button): ?>
		<a href="<?= $link; ?>" class="button round"><?= $button; ?></a>
	<?php endif; ?>
</div>
