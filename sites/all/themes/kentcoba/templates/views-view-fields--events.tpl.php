<?php if (empty($fields['ks_is_featured']->content)): ?>
	<?php /*
	<?php $dt = new DateTime($fields['ks_start_date']->content); ?>
	<?php $url = url('node/' . $fields['nid']->content); ?>
	<article class="event-summary" itemscope itemtype="http://schema.org/Event" onclick="location.href='<?= $url ?>'">
		<time itemprop="startDate" content="<?= $dt->format('U'); ?>">
			<?= $dt->format('D'); ?> <span class="dd"><?= $dt->format('d'); ?></span> <?= $dt->format('M'); ?>
		</time>
		<div class="event-content">
			<a itemprop="url" href="<?= $url; ?>" class="color5">
				<span class="dotted-after dotted-color5" itemprop="name"><?= $fields['title']->content; ?></span>
			</a>
			<span class="time"><?= $dt->format('h:i a'); ?></span>
			<span class="location" itemprop="location" itemscope=""><?= $fields['ks_location']->content; ?></span>
		</div>
	</article>
 	*/ ?>
<?php endif; ?>
