<?php

$renderer = $view->style_plugin->row_plugin;
if (isset($view->exposed_input['monday'])) {
	$monday = new DateTime($view->exposed_input['monday']);
	$sunday = clone $monday;
	$sunday->modify('+6 days');
} else {
	$monday = new DateTime('Monday this week');
	$sunday = new DateTime('Sunday this week');
}

?>

<div class="panel-pattern <?= $classes; ?>">
	<div class="row collapse events-intro-home">

		<div class="events-filters" data-view-type="block">
			<div class="wrap">
				<h1 class="subheader dotted-after dotted-color6 color6 h-center">Events</h1>
				<a href="#" id="selected-department" data-dropdown="events-department-dropdown" class="button dropdown round color2">All Events</a>
				<?= $exposed; ?>

				<div id="event-date-filter" class="button round split-action color2" data-monday="<?= $monday->format('U'); ?>" data-domid="<?= $dom_id; ?>" data-view-display="block">
					<a data-dir="back" href="#"><span class="i-arrow-left-white">&lt;</span></a>
					<?= $monday->format('F j'); ?> - <?= $sunday->format('F j'); ?>
					<a data-dir="forward" href="#"><span class="i-arrow-right-white">&gt;</span></a>
				</div>
			</div>
		</div>

		<div class="flexslider-home-events-container flexslider-controls-color2">
			<?php if ($view->useful): ?>
			<div class="flexslider flexslider-home-events">
				<ul class="slides">
					<li>
						<?php foreach ($view->useful as $id => $row): ?>
							<?php if (($id > 0) && ($id % 6 == '0')): ?>
								</li><li>
							<?php endif; ?>
							<?php $url = url('node/' . $row['nid']); $dt = $row['start_date']; ?>
							<article class="event-summary" itemscope itemtype="http://schema.org/Event" onclick="location.href='<?= $url ?>'">
								<time itemprop="startDate" content="<?= $dt->format('U'); ?>">
									<?= $dt->format('D'); ?> <span class="dd"><?= $dt->format('d'); ?></span> <?= $dt->format('M'); ?>
								</time>
								<div class="event-content">
									<a itemprop="url" href="<?= $url; ?>" class="color5">
										<span class="dotted-after dotted-color5" itemprop="name"><?= $row['title']; ?></span>
									</a>
									<span class="time"><?= $dt->format('h:i a'); ?></span>
									<span class="location" itemprop="location" itemscope=""><?= $row['location']; ?></span>
								</div>
							</article>

						<?php endforeach; ?>
					</li>
				</ul>
			</div>
			<?= l('See more Events', 'events', array('attributes' => array('class' => array('button', 'round', 'color1')))); ?>
			<?php else: ?>
			<p>Sorry, no results found.</p>
			<?php endif; ?>
		</div>

		<div class="flexslider-home-events-container-small">
			<h1 class="subheader color6 h-center dotted-after dotted-color6">Events</h1>
			<?php if ($view->useful): ?>
			<div class="flexslider flexslider-home-events-small">
				<ul class="slides">
					<?php foreach ($view->useful as $row): ?>
					<li>
						<?php $url = url('node/' . $row['nid']); $dt = $row['start_date']; ?>
						<article class="event-summary" itemscope itemtype="http://schema.org/Event" onclick="location.href='<?= $url ?>'">
							<time itemprop="startDate" content="<?= $dt->format('U'); ?>">
								<?= $dt->format('D'); ?> <span class="dd"><?= $dt->format('d'); ?></span> <?= $dt->format('M'); ?>
							</time>
							<div class="event-content">
								<a itemprop="url" href="<?= $url; ?>" class="color5">
									<span class="dotted-after dotted-color5" itemprop="name"><?= $row['title']; ?></span>
								</a>
								<span class="time"><?= $dt->format('h:i a'); ?></span>
								<span class="location" itemprop="location" itemscope=""><?= $row['location']; ?></span>
							</div>
						</article>
					</li>
					<?php endforeach; ?>
				</ul>
			</div>
			<?= l('See more Events', 'events', array('attributes' => array('class' => array('button', 'round', 'color1')))); ?>
			<?php else: ?>
			<p>Sorry, no results found.</p>
			<?php endif; ?>
		</div>


	</div>
</div>
