<?php

$external = $fields['ks_is_external']->content == 1 ? true : false;
$link = (!empty($fields['ks_link']->content) && $external) ? $fields['ks_link']->content : $fields['path']->content;
$display_link = ksGetDisplayLink($link);

?>
<li>
	<article class="news-summary-slider">
		<div class="news-photo" style="background-image: url('<?= $fields['uri']->content; ?>')"></div>
		<div class="news-overlay"></div>
		<div class="news-content">
			<time class="round"><?= $fields['ks_date']->content; ?></time>
			<h2 class="dotted-after dotted-color6 h-center"><a href="#"><?= $fields['title']->content; ?></a></h2>
			<span class="news-domain"><?= $display_link; ?></span>
		</div>
		<a href="<?= $link; ?>" class="button round color4 b-readmore" <?= $external ? 'target="_blank"' : ''; ?>>Read More</a>
	</article>
</li>
