<?php

$did = !empty($view->selected_department) ? $view->selected_department['id'] : 'All';
$dname = !empty($view->selected_department) ? ellipsis($view->selected_department['name'], 30) : 'Areas';

$sid = !empty($view->selected_name_group) ? $view->selected_name_group['id'] : 'All';
$sname = (!empty($view->selected_name_group) && $view->selected_name_group['name'] != 'All') ? $view->selected_name_group['name'] : 'Alpha';

?>
<div class="<?= $classes; ?>">
	<div class="staff-filters" style="margin-bottom: 20px;">
		<a href="#" id="selected-department" data-dropdown="staff-department-dropdown" data-department="<?= $did; ?>" class="button dropdown round color2"><?= $dname; ?></a>
		<a href="#" id="selected-names" data-dropdown="staff-names-dropdown" class="button dropdown round color2" data-namegroup="<?= $sid; ?>"><?= $sname; ?></a>
		<?= $view->combined_select; ?>
		<?= $view->names_select; ?>
	</div>
	<?= $rows; ?>
</div>
