<?php

$external = $fields['ks_is_external']->content == 1 ? true : false;
$link = (!empty($fields['ks_link']->content) && $external) ? $fields['ks_link']->content : $fields['path']->content;
$display_link = ksGetDisplayLink($link);

?>
<article class="news-summary">
	<?= $fields['ks_image']->content; ?>
	<div class="news-overlay"></div>
	<div class="news-content">
		<time class="round"><?= $fields['ks_date']->content; ?></time>
		<h2 class="h-center <?= ($external) ? 'dotted-after dotted-color5' : ''; ?>"><?= $fields['title']->content; ?></h2>
		<?php if ($external): ?>
			<span class="news-domain"><?= $display_link; ?></span>
		<?php endif; ?>
	</div>
	<a href="<?= $link; ?>" class="button round color4 b-readmore" <?= ($external) ? 'target="_blank"' : ''; ?>>Read More</a>
</article>
