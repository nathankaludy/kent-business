<?php

$link = $ks_link[0]['value'];
$external = $ks_is_external[0]['value'];

?>

<article class="news<?php if (empty($ks_image[0]['uri'])): ?> no-image<?php endif; ?>">
	<?php if (!empty($ks_image[0]['uri'])): ?>
		<img class="large photo" src="<?= file_create_url($ks_image[0]['uri']); ?>" alt=""/>
	<?php endif; ?>
	<div class="content-body">
		<?php if (!empty($ks_image[0]['uri'])): ?>
			<time class="round"><?= $dates['date'][0]->format('m/d/Y'); ?></time>
		<?php else: ?>
			<time class="round no-image"><?= $dates['date'][0]->format('m/d/Y'); ?></time>
		<?php endif; ?>
		<h1 class="dotted-after dotted-color2 color8"><?= $title; ?></h1>
		<?= render($content['body']); ?>
		<?php if ($link && !$external): ?>
			<a class="button large round color0 expand" href="<?= $link; ?>">Read More</a>
		<?php endif; ?>
	</div>
</article>