<div style="padding-bottom: 2em">
	<span></span>
	<h2><?= $view->display['featured_researcher']->display_options['title']; ?></h2>

	<div class="row gutter">
		<?php foreach ($rows as $row): ?>
			<?= $row; ?>
		<?php endforeach; ?>
	</div>
</div>
