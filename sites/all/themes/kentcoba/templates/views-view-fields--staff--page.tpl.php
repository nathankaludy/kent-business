<?php

$name = $fields['title']->content;
if (!empty($fields['ks_staff_suffix']->content)) {
	$name .= ", {$fields['ks_staff_suffix']->content}";
}

?>
<div class="photo"><?= $fields['ks_profile_picture']->content; ?></div>
<h2><?= $name; ?></h2>
<strong class="profile-job-title"><?= $fields['ks_staff_rank']->content; ?></strong><br>
<?= $fields['ks_phone_number']->content; ?><br>
<a href="mailto:<?= $fields['ks_email']->content; ?>" class="button-text-email"><?= $fields['ks_email']->content; ?></a>
