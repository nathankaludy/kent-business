<div class="large-8 medium-8 small-16 columns">
	<?php if (!empty($fields['ks_image']->content)): ?>
		<div class="photo-fixed-height">
			<?= $fields['ks_image']->content; ?>
		</div>
	<?php endif; ?>
	<article class="block-summary">
		<h3><?= $fields['title']->content; ?></h3>
		<p><?= $fields['body']->content; ?></p>
		<a href="<?= $base_url . $fields['path']->content; ?>" class="button-text color2">Learn More</a>
	</article>
</div>
