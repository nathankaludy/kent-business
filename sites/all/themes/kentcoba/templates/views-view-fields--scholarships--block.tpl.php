<div class="panel panel-photo-left">
	<div class="panel-photo" style="background-image: url(<?= $fields['uri']->content; ?>)"></div>
	<div class="panel-content">
		<h2><?= $fields['title']->content; ?></h2>
		<?= $fields['body']->content; ?>
		<a href="<?= $fields['field_link_target']->content; ?>" class="button round color6"><?= $fields['field_link_text']->content; ?></a>
	</div>
</div>
