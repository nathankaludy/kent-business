<article class="video-full">
	<div>
		<h4><?= $view->data['title']; ?></h4>
		<?= $view->data['body']; ?>
		<?php if (!empty($view->data['video'])): ?>
			<a href="#" class="button round" data-reveal-id="videoModal">Play Video <i class="i-play-white"></i></a>
		<?php endif; ?>
		<div class="video-photo" style="background-image: url(<?= $view->data['image']; ?>);"></div>
	</div>
</article>

<div class="panel panel-small even-4">
	<?php $secondary_nav = menu_navigation_links('menu-secondary-nav'); ?>
	<div class="row">
		<?= theme('links__menu_secondary_nav', array('links' => $secondary_nav)); ?>
	</div>
</div>

<div class="reveal-modal large" id="videoModal" style="display: none;">
	<a class="close-reveal-modal">&#215;</a>
	<div class="flex-video">
		<?= $view->data['video']; ?>
	</div>
</div>
