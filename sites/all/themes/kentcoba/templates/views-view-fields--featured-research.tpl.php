<article class="paper-summary">
	<strong><?= $fields['ks_author']->content; ?>, <?= $fields['ks_date']->content; ?></strong>
	<a href="<?= $fields['field_link_target']->content; ?>" target="_blank"><?= $fields['title']->content; ?></a>
	<?= $fields['field_publication_name']->content; ?>
</article>
