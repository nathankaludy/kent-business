<!DOCTYPE html>
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8">
    <?php print $head; ?>
    <title><?php print $head_title; ?></title>
    <meta name="viewport" content="width=device-width">

    <script>var env = "<?= $GLOBALS['base_url'] . '/' . request_path(); ?>";</script>
    <?php print $styles; ?>
    <?php print $scripts; ?>
    <!--[if LT IE 9]><script src="/<?php print $directory; ?>/assets/js/lt-ie9.js"></script><![endif]-->

    <script src="//use.typekit.net/lnd8wnu.js"></script>
    <script>try{Typekit.load();}catch(e){}</script>
	<script>
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		ga('create', 'UA-59561760-1', 'auto');
		ga('send', 'pageview');
	</script>
    
	
	<script type="text/javascript">
(function(a,e,c,f,g,b,d){var h={ak:"973022658",cl:"m5JPCMvhzlsQwsv8zwM"};a[c]=a[c]||function(){(a[c].q=a[c].q||[]).push(arguments)};a[f]||(a[f]=h.ak);b=e.createElement(g);b.async=1;b.src="//www.gstatic.com/wcm/loader.js";d=e.getElementsByTagName(g)[0];d.parentNode.insertBefore(b,d);a._googWcmGet=function(b,d,e){a[c](2,b,h,d,null,new Date,e)}})(window,document,"_googWcmImpl","_googWcmAk","script");
</script>

  
</head>

<body class="<?php print $classes; ?>" <?php print $attributes;?>>

    <!--Kent Header-->
    <header role="banner" class="l-header small-16 medium-16 large-16">
        <div class="header-top-container small-16 medium-16 large-16">
            <section class="header-top-left-region small-16 medium-16 large-10 columns">
                <div>
                    <div id="block-kent-state-blocks-campus-secondary-menu" class="block block-kent-state-blocks block-kent-state-blocks-campus-secondary-menu">
                        <div class="content">
                            <a href="https://login.kent.edu" class="flashline-login-link"><i class="fi-lock"></i>&nbsp; Flashline Login</a>
                            <a href="http://www.kent.edu/calendars">Calendars</a>
                            <a href="/" aria-controls="drop38761" data-dropdown="drop38761" aria-expanded="false" data-options="" class="active">Phone Directory</a>
                            <div id="drop38761" class="f-dropdown" data-dropdown-content="" aria-hidden="true" tabindex="-1">
                                <ul class="no-bullet">
                                    <li><a href="https://keys.kent.edu/ePROD/bwgkphon.P_EnterStaffCriteria">Faculty &amp; Staff</a>
                                    </li>
                                    <li><a href="https://keys.kent.edu/ePROD/bwgkphon.P_EnterEmeritiCriteria">Emeriti or Retiree</a>
                                    </li>
                                    <li><a href="https://keys.kent.edu/ePROD/bwgkphon.P_DeptDir">All Departments</a>
                                    </li>
                                </ul>
                            </div>
                            <a href="http://www.kent.edu/maps">Maps &amp; Directions</a> </div>
                    </div>
                </div>
            </section>
            <section class="header-top-right-region small-16 medium-16 large-6 columns">
                <div>
                    <section class="block block-google-appliance block-google-appliance-ga-block-search-form">
                        <form action="http://www.kent.edu/gsearch" method="post" id="google-appliance-block-form" accept-charset="UTF-8">
                            <div>
                                <div class="row collapse">
                                    <div class="small-14 medium-14 large-14 columns">
                                        <div class="form-item form-type-textfield form-item-search-keys" role="application">
                                            <label class="element-invisible" for="edit-search-keys">Enter the terms you wish to search for. </label>
                                            <input class="form-text" type="text" name="search_keys" value="" size="15" maxlength="128" autocomplete="OFF">
                                        </div>
                                    </div>
                                    <div class="small-2 medium-2 large-2 columns">
                                        <input type="submit" class="button-search">
                                    </div>
                                </div>
                                <div class="row search-links">
                                    <a href="http://www.kent.edu/index">A-Z Index</a> |
                                    <a href="http://kentstate.askadmissions.net/ask.aspx?ref=ksuheader">Ask KSU</a>
                                </div>
                            </div>
                        </form>
                    </section>
                    <div id="block-kent-state-blocks-campus-donate" class="block block-kent-state-blocks block-kent-state-blocks-campus-donate">
                        <div class="content">
                            <a href="https://secure2.convio.net/ksu/site/SPageNavigator/COBA_designated_giving_form.html/#/"><img typeof="foaf:Image" src="http://du1ux2871uqvu.cloudfront.net/sites/default/files/GiveToKentState_2.png" alt="Give to Kent State" title="Give to Kent State">
                            </a>
                        </div>
                    </div>
                </div>
            </section>
            <section class="campus-header-region small-16 medium-16 large-16 left">
                <div>
                    <div id="block-kent-state-blocks-campus-logo" class="block block-kent-state-blocks block-kent-state-blocks-campus-logo">
                        <div class="content">
                            <a href="http://www.kent.edu/" class="active"><!--[if gte IE 9]><!----><img src="http://du1ux2871uqvu.cloudfront.net/sites/default/files/Kent-State-Logo-White.svg" title="" alt=""><!--<![endif]--><!--[if lt IE 9]><img src="http://du1ux2871uqvu.cloudfront.net/sites/default/files/Kent-State-Logo-White-Fallback%401x.png" title="" alt=""><![endif]--></a>
                        </div>
                    </div>
                </div>
            </section>

            <section class="group-header-region small-16 medium-16 large-16 left">
                <div>
                    <div id="block-kent-state-blocks-group-foundation-topbar" class="block block-kent-state-blocks block-kent-state-blocks-group-foundation-topbar">
                        <div class="content">
                            <nav class="top-bar" data-index="1">
                                <ul class="title-area">
                                    <li class="name"><h1><a href="http://www.kent.edu" class="active"><!--[if gte IE 9]><!----><img src="http://du1ux2871uqvu.cloudfront.net/sites/default/files/Kent-State-Logo-White.svg" title="" alt=""><!--<![endif]--><!--[if lt IE 9]><img src="http://du1ux2871uqvu.cloudfront.net/sites/default/files/Kent-State-Logo-White-Fallback%401x.png" title="" alt=""><![endif]--></a></h1></li>
                                    <li class="toggle-topbar menu-icon"><a href="#">Menu</a></li>
                                </ul>
                                <section class="top-bar-section">
                                    <ul id="main-menu" class="main-nav left">
                                        <li class="first leaf flashline-login-link"><a href="https://login.kent.edu" class="flashline-login-link" alt=""><i class="fi-lock"></i> Flashline Login</a></li>
                                        <li class="leaf"><a href="http://www.kent.edu/calendars">Calendars</a></li>
                                        <li class="leaf"><a href="http://www.kent.edu/">Phone Directory</a>
                                            <?php /* <ul class="dropdown">
                                                <li class="show-for-medium-down"><a href="/">Phone Directory</a></li>
                                                <li class="first leaf"><a href="https://keys.kent.edu/ePROD/bwgkphon.P_EnterStaffCriteria" title="">Faculty &amp; Staff</a></li>
                                                <li class="leaf"><a href="https://keys.kent.edu/ePROD/bwgkphon.P_EnterEmeritiCriteria" title="">Emeriti or Retiree</a></li>
                                                <li class="last leaf"><a href="https://keys.kent.edu/ePROD/bwgkphon.P_DeptDir">All Departments</a></li>
                                            </ul> */?>
                                        </li>
                                        <li class="last leaf"><a href="http://www.kent.edu/maps" title="">Maps &amp; Directions</a></li>
                                    </ul>
                                </section>
                            </nav>
                        </div>
                    </div>
                </div>
            </section>


        </div>
    </header>
    <!--/Kent Header-->


    <?php print $page_top; ?>
    <?php print $page; ?>
    <?php print $page_bottom; ?>


    <!--Kent Footer-->
    <footer id="kent-footer" role="contentinfo" class="l-footer small-16 medium-16 large-16 columns">
        <div class="footer-rule-region large-12">
          <hr>
        </div>

        <section class="footer-middle-region small-16 medium-16 large-16 left">
            <div class="small-16 medium-16 large-16 columns">
                <section class="block block-kent-state-blocks block-kent-state-blocks-campus-logo-footer">
                    <a href="http://www.kent.edu" class="active"><!--[if gte IE 9]><!----><img src="http://du1ux2871uqvu.cloudfront.net/sites/default/files/Kent-State-Logo-White.svg" title="" alt=""><!--<![endif]--><!--[if lt IE 9]><img src="http://du1ux2871uqvu.cloudfront.net/sites/default/files/Kent-State-Logo-White-Fallback%401x.png" title="" alt=""><![endif]--></a>
                </section>

                <section class="block block-kent-state-blocks block-kent-state-blocks-campus-social-media">
                    <ul class="social-media-footer">
                        <li>
                            <a href="https://www.pinterest.com/kentstate" class="social-media-network-pinterest">
                                <i class="fi-social-pinterest"></i>
                            </a>
                        </li>
                        <li>
                            <a href="https://www.flickr.com/photos/kentstate" class="social-media-network-flickr">
                                <i class="fi-social-flickr"></i>
                            </a>
                        </li>
                        <li>
                            <a href="https://www.facebook.com/kentstate" class="social-media-network-facebook">
                                <i class="fi-social-facebook"></i>
                            </a>
                        </li>
                        <li>
                            <a href="https://www.twitter.com/kentstate" class="social-media-network-twitter">
                                <i class="fi-social-twitter"></i>
                            </a>
                        </li>
                        <li>
                            <a href="https://www.youtube.com/KentStateTV" class="social-media-network-youtube">
                                <i class="fi-social-youtube"></i>
                            </a>
                        </li>
                        <li>
                            <a href="http://www.linkedin.com/company/kent-state-university" class="social-media-network-linkedin">
                                <i class="fi-social-linkedin"></i>
                            </a>
                        </li>
                        <li>
                            <a href="https://plus.google.com/+kentstate" class="social-media-network-googleplus">
                                <i class="fi-social-google-plus"></i>
                            </a>
                        </li>
                        <li>
                            <a href="https://www.instagram.com/kentstate" class="social-media-network-instagram">
                                <i class="fi-social-instagram"></i>
                            </a>
                        </li>
                        <li>
                            <a href="http://www.kent.edu/social-media" class="more-link">
                                <i class="fi-plus"></i>
                            </a>
                        </li>
                    </ul>
                </section>

                <section class="block block-kent-state-blocks block-kent-state-blocks-campus-footer-menu">
                    <ul class="menu">
                        <li class="first leaf"><a href="http://www.kent.edu/colleges-and-schools" title="">Colleges &amp; Schools</a></li>
                        <li class="leaf"><a href="https://keys.kent.edu:44220/ePROD/bwgkphon.P_DeptDir" title="">Administrative Offices</a></li>
                        <li class="leaf"><a href="https://jobs.kent.edu/applicants/jsp/shared/frameset/Frameset.jsp?time=1416889656548" title="">Jobs &amp; Employment</a></li>
                        <li class="leaf"><a href="http://support.kent.edu/" title="">Technology Support</a></li>
                        <li class="leaf"><a href="http://www.kent.edu/police" title="">Emergency Information</a></li>
                        <li class="leaf"><a href="http://www.kent.edu/parents" title="">For Parents</a></li>
                        <li class="last leaf"><a href="http://www.kent.edu/news" title="">For the Media</a></li>
                    </ul>
                </section>
            </div>
        </section>

        <section class="footer-bottom-region small-16 medium-16 large-16 left">
            <div class="small-16 medium-16 large-14 large-centered columns">
                <div id="block-kent-state-blocks-campus-footer" class="block block-kent-state-blocks block-kent-state-blocks-campus-footer">
                    <a class="go-to-top-button" href="#"><span>Back to Top</span></a>
                    <div class="content">
                        <div class="container">
                            <h6>Phone</h6>
                            <span class="phone">330-672-3000</span>
                        </div>
                        <div class="container">
                            <h6>Street Address</h6>
                            <span class="street-address">800 E. Summit St., Kent, OH 44240</span>
                        </div>
                        <div class="container">
                            <h6>Mailing Address</h6>
                            <span class="mailing-address">P.O. Box 5190, Kent, OH 44242-0001</span>
                        </div>
                        <div class="container">
                            <h6>Email</h6>
                            <span class="email">
                                <a href="mailto:info@kent.edu">info@kent.edu</a>
                            </span>
                        </div>
                        <div class="body">
                            <p>As a top Ohio undergraduate and graduate school, Kent State's eight campuses offer the resources of a large university with the friendly atmosphere of a liberal arts college. Enroll today to start pursuing your future at one of the best colleges in Ohio. We’ve been educating graduates for over 100 years; join us today.</p>
                            <p>Copyright 2015 Kent State University</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>

    </footer>
    <!--/Kent Footer-->
    
     <script>
window.onload= function() { _googWcmGet('number', '330-672-EMBA'); };
</script> 
    
</body>
</html>
