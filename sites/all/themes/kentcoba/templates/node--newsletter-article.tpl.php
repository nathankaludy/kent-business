<article class="newsletter-article">
	<div class="content-body">
		<h2>Current Issue</h2>
		<h3><?= $field_associated_newsletter[0]['taxonomy_term']->name; ?></h3>
	</div>

	<?php if (!empty($field_image[0]['uri'])): ?>
	<img class="large photo active" src="<?= file_create_url($field_image[0]['uri']); ?>" alt=""/>
	<div class="content-body">
	<?php else: ?>
	<div class="content-body no-image">
	<?php endif; ?>
	
		<h1 class="dotted-after dotted-color2 color8"><?= $title; ?></h1>
		<?= render($content['body']); ?>
	</div>
</article>
