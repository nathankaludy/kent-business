<article class="profile-full-summary">
	<img src="<?= $fields['uri']->content; ?>" alt="" class="photo active">

	<div class="profile-content">
		<h2><?= $fields['title']->content; ?></h2>
		<strong class="profile-job-title"><?= $fields['ks_staff_rank']->content; ?></strong><br>
		<?= $fields['ks_phone_number']->content; ?><br>
		<a href="mailto:<?= $fields['ks_email']->content; ?>" class="button-text-email"><?= $fields['ks_email']->content; ?></a>

		<p><?= $fields['ks_staff_bio']->content; ?></p>
		<a href="<?= drupal_get_path_alias(url('node/' . $row->nid)); ?>" class="button-text color2">See full bio</a>
	</div>

	<a href="/staff" class="button round">Faculty Directory</a>
</article>
