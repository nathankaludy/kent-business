<article id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?>"<?php print $attributes; ?>>

	<?php print $user_picture; ?>

	<div<?php print $content_attributes; ?>>
		<?php
		// We hide the comments and links now so that we can render them later.
		hide($content['comments']);
		hide($content['links']);
		print render($content);
		?>
	</div>

	<?php print render($content['links']); ?>

	<?php print render($content['comments']); ?>

</article>
