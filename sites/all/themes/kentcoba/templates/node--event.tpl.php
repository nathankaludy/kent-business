<article class="event">
	<?php if (!empty($node->ks_image)): ?>
		<img src="<?= file_create_url($node->ks_image['und'][0]['uri']); ?>" class="large photo active">
	<?php endif; ?>

	<div class="content-body">
		<h1 class="dotted-after dotted-color2 color8"><?= render($title); ?></h1>
		<?= render($content['body']); ?>

		<div class="event-dates">
			<div class="col">
				<h3><?php if($dates['start'][0] != $dates['end'][0]): ?>Start <?php endif; ?>Date</h3>
				<div class="block-date">
					<div class="block-date-date">
						<?= $dates['start'][0]->format('l'); ?> &nbsp;<strong><?= $dates['start'][0]->format('m/d/Y'); ?></strong>
					</div>
					<div class="block-date-time">
						<?= $dates['start'][0]->format('g:i A'); ?>
					</div>
				</div>
			</div>

			<?php if($dates['start'][0] != $dates['end'][0]): ?>
			<div class="col">
				<h3>End Date</h3>
				<div class="block-date">
					<div class="block-date-date">
						<?= $dates['end'][0]->format('l'); ?> &nbsp;<strong><?= $dates['end'][0]->format('m/d/Y'); ?></strong>
					</div>
					<div class="block-date-time">
						<?= $dates['end'][0]->format('g:i A'); ?>
					</div>
				</div>
			</div>
			<?php endif; ?>

			<div class="col">
				<h3>Location</h3>
				<div class="block-location">
					<?= render($content['ks_location']); ?>
				</div>
			</div>
		</div>


	</div>
</article>
