<address>
	<h4 class="subheader dotted-before dotted-color7 color6">
		<?php if (isset($fields['ks_link']) && !empty($fields['ks_link']->content)): ?>
			<a href="<?= url($fields['ks_link']->content, array('absolute' => true)); ?>">
				<?= $fields['title']->content; ?>
			</a>
		<?php else: ?>
			<?= $fields['title']->content; ?>
		<?php endif; ?>
	</h4>

	<?php if (isset($fields['ks_phone_number']) && !empty($fields['ks_phone_number']->content)): ?>
		<?= $fields['ks_phone_number']->content; ?><br>
	<?php endif; ?>

	<?php if (isset($fields['ks_address']) && !empty($fields['ks_address']->content)): ?>
		<?= nl2br($fields['ks_address']->content); ?><br>
	<?php endif; ?>

	<?php if (isset($fields['ks_email']) && !empty($fields['ks_email']->content)): ?>
		<a href="mailto:<?= $fields['ks_email']->content; ?>" class="button-text color2"><?= $fields['ks_email']->content; ?></a>
	<?php endif; ?>
</address>
