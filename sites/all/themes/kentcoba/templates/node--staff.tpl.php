<article class="profile">
	<div class="photo active"><?= render($content['ks_profile_picture']); ?></div>

	<div class="intro">
		<h1><?= render($content['full_name']); ?></h1>
		<strong class="profile-job-title"><?= $ks_staff_rank[0]['value']; ?><br><?= render($content['ks_department']); ?></strong><br>
		<?= $ks_phone_number[0]['value']; ?><br>
		<a href="mailto:<?= $ks_email[0]['value']; ?>" class="button-text-email"><?= $ks_email[0]['value']; ?></a>
		<?php if (!empty($ks_link[0]['value'])): ?>
			<br>
			<a href="<?= $ks_link[0]['value']; ?>" class="button-text-email"><?= $ks_link[0]['value']; ?></a>
		<?php endif; ?>
	</div>

	<?php if(!empty($ks_staff_bio_education[0]['value'])): ?>
	<section class="first">
		<h3>Education</h3>
		<p><?= nl2br($ks_staff_bio_education[0]['value']); ?></p>
	</section>
	<?php endif; ?>

	<?php if(!empty($ks_staff_bio[0]['value'])): ?>
	<section>
		<h3>Biography</h3>
		<p><?= nl2br($ks_staff_bio[0]['value']); ?></p>
		<?php if (!empty($field_link_to_cv[0]['value'])): ?>
		<a href="<?= $field_link_to_cv[0]['value']; ?>" class="button-text color2">Curriculum Vitae</a>
		<?php endif; ?>
	</section>
	<?php endif; ?>

	<?php if(!empty($ks_staff_bio_expertise[0]['value'])): ?>
	<section>
		<h3>Areas Of Expertise</h3>
		<p><?= nl2br($ks_staff_bio_expertise[0]['value']); ?></p>
	</section>
	<?php endif; ?>

	<?php if (!empty($ks_staff_bio_publications)): ?>
	<section>
		<h3>Selected Publications</h3>
		<div class="list-papers">
			<?php foreach ($ks_staff_bio_publications as $paper): ?>
				<article class="paper-summary">
					<p><?= $paper['value']; ?></p>
				</article>
			<?php endforeach; ?>
		</div>
	</section>
	<?php endif; ?>

	<?php if (!empty($ks_staff_bio_research)): ?>
	<section>
		<h3>Working Papers And Research</h3>
		<div class="list-papers">
			<?php foreach ($ks_staff_bio_research as $paper): ?>
				<article class="paper-summary">
					<p><?= $paper['value']; ?></p>
				</article>
			<?php endforeach; ?>
		</div>
	</section>
	<?php endif; ?>
</article>
