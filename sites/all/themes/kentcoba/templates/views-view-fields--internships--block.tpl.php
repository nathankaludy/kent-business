<div class="panel panel-color2">
	<h2><?= $fields['title']->content; ?></h2>
	<?= $fields['body']->content; ?>
	<a href="<?= $fields['field_link_target']->content; ?>" class="button round color6"><?= $fields['field_link_text']->content; ?></a>
</div>
