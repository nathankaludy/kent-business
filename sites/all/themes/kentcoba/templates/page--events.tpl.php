<?= theme('main_menu', array('main_menu' => $main_menu, 'site_name' => $site_name)); ?>

<?php print render($page['content']); ?>

<div class="panel-color2 panel-small panel-center">
	<?php $tertiary_nav = menu_navigation_links('menu-tertiary-nav'); ?>
	<div class="row">
		<?= theme('links__menu_tertiary_nav', array('links' => $tertiary_nav)); ?>
	</div>
</div>


<!-- COBA Footer -->
<footer id="coba-footer">
	<div class="row collapse">
		<div class="large-4 large-push-1 medium-4 medium-push-1 columns">
			<?php print render($page['footer_column1']); ?>
			<?php print render($page['footer_social']); ?>
		</div>
		<div class="large-9 large-pull-1 medium-9 medium-pull-1 columns footer-contact">
			<?php print render($page['footer_column2']); ?>
		</div>
	</div>

	<div class="row footer-logos">
		<?php print render($page['footer_logos']); ?>
	</div>
</footer>
<!-- /COBA Footer -->
