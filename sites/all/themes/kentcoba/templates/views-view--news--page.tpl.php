<?php

$count = $view->offset + count($view->result);
$selected = !empty($view->selected_department) ? ellipsis($view->selected_department['name'], 30) : 'Department';
$sid = !empty($view->selected_department) ? $view->selected_department['id'] : 'All';

?>
<div class="<?= $classes; ?>">
	<div class="title-dropdown">
		<h1>Sort By</h1>
		<a href="#" id="selected-department" data-dropdown="news-department-dropdown" data-department="<?= $sid; ?>" class="button dropdown round color2"><?= $selected; ?></a>
		<?= $view->combined_select; ?>
		<a href="#" id="selected-year" data-dropdown="news-year-dropdown" class="button dropdown round color2"></a>
		<span class="loading">
			<span class="loading1"></span>
			<span class="loading2"></span>
			<span class="loading3"></span>
		</span>
		<?= $exposed; ?>
	</div>

	<div class="news-items">
		<div class="news-items-sizer"></div>
		<div class="news-items-gutter"></div>
		<?= $rows; ?>
	</div>
	<?php if ($view->total_rows > $count): ?>
	<a href="#" id="news-load-more" class="button large round color0 expand" data-rows="<?= $view->total_rows; ?>" data-offset="<?= $count; ?>">
		Load More
		<span class="loading">
			<span class="loading1"></span>
			<span class="loading2"></span>
			<span class="loading3"></span>
		</span>
	</a>
	<?php endif; ?>
</div>
