<?php $dept = strtolower(str_replace(' ', '-', $fields['ks_department']->content)); ?>
<article class="block-summary">
	<h3><?= $fields['name']->content; ?></h3>
	<p><?= $fields['description']->content; ?></p>
	<a href="/departments/<?= $dept; ?>" class="button-text color2">View Curriculum</a>
</article>
