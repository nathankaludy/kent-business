<?php $dept = strtolower(str_replace(' ', '-', $fields['ks_department']->content)); ?>
<?php $link = !preg_match('#^/node#', $fields['path']->content) ? $fields['path']->content : '/departments/' . $dept; ?>
<article class="block-summary">
	<h3><?= $fields['title']->content; ?></h3>
	<p><?= $fields['body']->content; ?></p>
	<a href="<?= $link; ?>" class="button-text color2">Read More</a>
</article>
