<div class="archived-issues">
	<h2 class="h2-5">Archived Issues</h2>

	<div class="accordion" data-section="accordion">
		
		<?php
		$count = 0;
		foreach ($view->parsed as $month): ?>
		<section<?php if($count==0): ?> class="active"<?php endif; ?>>
			<div class="accordion-title" data-section-title>
				<h2><?= $month['name']; ?></h2>
				<a href="#" data-section-button class="button round small color1"><?php if($count==0): ?>Close<?php else: ?>Expand<?php endif; ?></a>
			</div>
			<div class="accordion-content"  data-section-content>
				<ul class="list-newsletter">
					<?php foreach ($month['items'] as $article): ?>
					<li><a href="<?= $article['link']; ?>"><?= $article['title']; ?></a></li>
					<?php endforeach; ?>
				</ul>
			</div>
		</section>
		<?php
		$count++;
		endforeach; ?>

	</div>

</div>
