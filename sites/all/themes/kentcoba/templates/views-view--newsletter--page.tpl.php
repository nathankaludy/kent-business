<?php

$featured = $view->parsed['featured'];
$dean     = $view->parsed['dean'];
$boxed    = $view->parsed['boxed'];
$other    = $view->parsed['other'];

$heading = theme_get_setting('newsletter_heading');
$blurb   = theme_get_setting('newsletter_blurb');
$link_text = theme_get_setting('newsletter_button_text');
$link_target = theme_get_setting('newsletter_button_target');

?>

<div class="content-body">
	<h1><?= $heading; ?></h1>
	<p><?= $blurb; ?></p>
	<a href="<?= $link_target; ?>" class="button round"><?= $link_text; ?></a>
</div>

<div class="current-issue">
	<h2 class="h2-5"><a name="Current Issue"></a>Current Issue</h2>
	<h3><?= $view->parsed['current']; ?></h3>
</div>

<?php /* Featured article */ ?>
<?php if (!empty($featured)): ?>
	<article class="news">
		<img class="large photo active" src="<?= $featured['image']; ?>">
		<div class="content-body">
			<h1 class="dotted-after dotted-color2 color8"><?= $featured['title']; ?></h1>
			<?= $featured['body']; ?>
			<a class="button-text color2" href="<?= $featured['link']; ?>">Read More</a>
		</div>
	</article>
<?php endif; ?>


<div class="current-news">
	<?php /* First four articles (with images) */ ?>
	<?php /* Message from the Dean */ ?>
	<?php if (!empty($dean)): ?>
		<article class="news-summary no-dates news-from-dean" style="background-image: url(<?= $dean['image']; ?>)">
			<div class="news-content">
				<h2 class="dotted-after dotted-color6 h-center">A Word From the Dean</h2>
				<p><?= $dean['title']; ?></p>
				<a href="<?= $dean['link']; ?>" class="button round small color4">Read More</a>
			</div>
		</article>
	<?php endif; ?>
	<?php foreach ($boxed as $article): ?>
		<article class="news-summary no-dates">
			<?php $img = file_create_url($article['image']); ?>
			<?php if(!empty($img)): ?>
			<div class="thumb-cover" style="background-image: url(<?= file_create_url($article['image']); ?>)"></div>
			<?php endif; ?>
			<div class="news-overlay"></div>
			<div class="news-content">
				<h2 class="h-center"><?= $article['title']; ?></h2>
			</div>
			<a href="<?= $article['link']; ?>" class="button round color4 b-readmore">Read More</a>
		</article>
	<?php endforeach; ?>
</div>

<div class="current-news-extra">
	<?php /* Any remaining articles */ ?>
	<?php foreach ($other as $article): ?>
		<article class="block-news-small">
			<h2 class="dotted-after dotted-color5"><a href="<?= $article['link']; ?>"><?= $article['title']; ?></a></h2>
			<p><?= $article['body']; ?></p>
			<a class="button-text color2" href="<?= $article['link']; ?>">Read More</a>
		</article>
	<?php endforeach; ?>
</div>

