<?php
// Swap out jQuery to use an updated version of the library.
function kentcoba_js_alter(&$javascript) {
	$javascript['misc/jquery.js']['data'] = 'http://code.jquery.com/jquery-1.11.0.min.js';

	// Required for Drupal admin overlay, which depends on jQuery.browser (removed in 1.9)
	drupal_add_js('http://code.jquery.com/jquery-migrate-1.2.1.js', array('option' => 'external', 'weight' => -19.988, 'group' => -100, 'every_page' => 1));

	// Remove JS
	if(user_is_logged_in() != 1) {
		unset($javascript['misc/jquery.ba-bbq.js']);
		unset($javascript['modules/overlay/overlay-parent.js']);
		unset($javascript['modules/contextual/contextual.js']);
		unset($javascript['misc/jquery.cookie.js']);
		unset($javascript['misc/progress.js']);
		unset($javascript['modules/overlay/overlay-parent.js']);
		unset($javascript['modules/contextual/contextual.js']);
		// unset($javascript['sites/all/modules/contrib/ctools/js/auto-submit.js']);
		// unset($javascript['sites/all/modules/contrib/views/js/base.js']);
		// unset($javascript['sites/all/modules/contrib/views/js/ajax_view.js']);
	}

	// unset($javascript['misc/jquery.form.js']); // Required for Views exposed filters
	unset($javascript['misc/ui/jquery.ui.core.min.js']);
	unset($javascript['sites/all/modules/contrib/views/js/ajax_view.js']);
	unset($javascript['misc/ajax.js']);
	unset($javascript['misc/ui/jquery.ui.core.min.js']);
	unset($javascript['sites/all/modules/contrib/admin_menu/admin_menu.js']);
}

// Remove all core Drupal css files
function kentcoba_css_alter(&$css) {
	// Remove core block stylesheet(s)
	unset($css[drupal_get_path('module', 'block') . '/block.css']);

	// Remove core date stylesheet(s)
	unset($css[drupal_get_path('module', 'date') . '/date_api/date.css']);
	unset($css[drupal_get_path('module', 'date') . '/date_popup/themes/datepicker.1.7.css']);

	// Remove core ctools stylesheet(s)
	unset($css[drupal_get_path('module', 'ctools') . '/css/ctools.css']);

	// Remove core fields stylesheet(s)
	unset($css[drupal_get_path('module', 'field') . '/theme/field.css']);

	// Remove core node stylesheet(s)
	unset($css[drupal_get_path('module', 'node') . '/node.css']);

	// Remove core system stylesheet(s)
	unset($css[drupal_get_path('module', 'system') . '/admin.css']);
	unset($css[drupal_get_path('module', 'system') . '/defaults.css']);
	unset($css[drupal_get_path('module', 'system') . '/system.css']);
	// unset($css[drupal_get_path('module', 'system') . '/system.base.css']);
	unset($css[drupal_get_path('module', 'system') . '/system.menus.css']);
	unset($css[drupal_get_path('module', 'system') . '/system.messages.css']);
	unset($css[drupal_get_path('module', 'system') . '/system.theme.css']);

	// Remove core user stylesheet(s)
	unset($css[drupal_get_path('module', 'user') . '/user.css']);

	// Remove core views stylesheet(s)
	unset($css[drupal_get_path('module', 'views') . '/css/views.css']);

	// Remove core search stylesheet(s)
	unset($css[drupal_get_path('module', 'search') . '/search.css']);

	// Remove core comments stylesheet(s)
	unset($css[drupal_get_path('module', 'comment') . '/comment.css']);

}

function kentcoba_preprocess_page(&$vars) {
	if ($vars['is_front'] === true) {
		// Remove the "No front page content" block
		unset($vars['page']['content']['system_main']['default_message']);
	}

	// Use specific layout for single events pages.
	if ($vars['node']->type == 'event') {
		$vars['theme_hook_suggestions'][] = 'page__node__events';
	}

	if ($vars['node']->type == 'page' || $vars['node']->type == 'degree') {
		$label   = ksExtractSidebarLabel($vars['page']['content']['system_main']);
		$markup  = ksExtractContentMarkup($vars['page']['content']['system_main']);
		$markup .= ksExtractBlockMarkup($vars['page']['content']);
		$links   = ksExtractSidebarLinks($markup);

		$sidebar = ksGenerateSideNav($links, $label);

		$vars['page']['sidebar']['#region'] = 'sidebar';
		$vars['page']['sidebar']['sidenav'] = array(
			'#markup' => $sidebar,
			'#weight' => -200,
		);
	}

	if ($vars['node']->type == 'event') {
		$previous = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '/events';
		$markup = '<h2 class="subheader dotted-after dotted-color5">Events</h2>'
			. '<a href="' . $previous . '" class="button-icon color2">'
			. '<i class="i-arrow-left-color2"></i>Back</a>';

		$vars['page']['sidebar']['#region'] = 'sidebar';
		$vars['page']['sidebar']['sidenav'] = array(
			'#markup' => $markup,
		);
	}

	if ($vars['node']->type == 'news') {
		$previous = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '/news';
		$markup = '<h2 class="subheader dotted-after dotted-color5">News</h2>'
			. '<a href="'. $previous . '" class="button-icon color2">'
			. '<i class="i-arrow-left-color2"></i>Back</a>';

		$vars['page']['sidebar']['#region'] = 'sidebar';
		$vars['page']['sidebar']['sidenav'] = array(
			'#markup' => $markup,
		);
	}

	if ($vars['node']->type == 'newsletter_article') {
		$view = views_get_view('newsletter');
		$markup = '<h2 class="subheader dotted-after dotted-color5">Newsletter</h2>'
			. '<a href="/'.$view->display['page']->display_options['path'].'" class="button-icon color2">'
			. '<i class="i-arrow-left-color2"></i>Back</a>';

		$vars['page']['sidebar']['#region'] = 'sidebar';
		$vars['page']['sidebar']['sidenav'] = array(
			'#markup' => $markup,
		);
	}

	if ($vars['node']->type == 'staff') {
		$markup = '<h2 class="subheader dotted-after dotted-color5">Faculty and Staff</h2>'
			. '<a href="/staff" class="button-icon color2">'
			. '<i class="i-arrow-left-color2"></i>Back</a>';

		$vars['page']['sidebar']['#region'] = 'sidebar';
		$vars['page']['sidebar']['sidenav'] = array(
			'#markup' => $markup,
		);
	}

	if (!isset($vars['node']) && isset($vars['page']) && isset($vars['page']['content']['views_newsletter-archived'])) {
		$vars['theme_hook_suggestions'][] = 'page__newsletter';
		$label   = 'Newsletter';
		$markup  = $vars['page']['content']['system_main']['main']['#markup'];
		$markup .= ksExtractBlockMarkup($vars['page']['content']);
		$links   = ksExtractSidebarLinks($markup);
		$sidebar = ksGenerateSideNav($links, $label);

		$vars['page']['sidebar']['#region'] = 'sidebar';
		$vars['page']['sidebar']['sidenav'] = array(
			'#markup' => $sidebar,
			'#weight' => -200,
		);
	}

	if (!isset($vars['node']) && $vars['page']['#views_contextual_links_info']['views_ui']['view_display_id'] == 'experts') {
		$label   = 'Experts';
		$markup  = $vars['page']['content']['system_main']['main']['#markup'];
		$markup .= ksExtractBlockMarkup($vars['page']['content']);
		$links   = ksExtractSidebarLinks($markup);
		$sidebar = ksGenerateSideNav($links, $label);

		$vars['page']['sidebar']['#region'] = 'sidebar';
		$vars['page']['sidebar']['sidenav'] = array(
			'#markup' => $sidebar,
			'#weight' => -200,
		);
	}
}

function kentcoba_preprocess_node(&$variables) {
	if ($variables['type'] == 'staff') {
		$variables['content']['full_name'] = array(
			'#markup' => ksGenerateFullName($variables),
		);
	}

	if (!array_key_exists('dates', $variables)) {
		$variables['dates'] = array();
	}

	if (array_key_exists('ks_date', $variables)) {
		foreach ($variables['ks_date'] as $key => $value) {
			$variables['dates']['date'][$key] = ksGetSaneDate($value);
		}
	}

	if (array_key_exists('ks_start_date', $variables)) {
		foreach ($variables['ks_start_date'] as $key => $value) {
			$dates = ksGetSaneStartEndDate($value);
			$variables['dates']['start'][$key] = $dates['start'];
			$variables['dates']['end'][$key]   = $dates['end'];
		}
	}

	if (array_key_exists('ks_end_date', $variables)) {
		foreach ($variables['ks_end_date'] as $key => $value) {
			$variables['dates']['end'][$key] = ksGetSaneDate($value);
		}
	}
}

function kentcoba_form_alter(&$form, &$form_state, $form_id) {
	if ($form['#id'] == 'views-exposed-form-events-block' || $form['#id'] == 'views-exposed-form-events-page-1') {
		$form['affiliation_filter']['#options']['All'] = 'All Events';
		$form['affiliation_filter']['#attributes']['class'] = array('button', 'dropdown', 'round', 'color2');
	}

	if ($form['#id'] == 'views-exposed-form-news-page') {
		$form['news_affiliation_filter']['#options']['All'] = 'All News';
	}
}

function kentcoba_links__system_main_menu(&$vars){
	// Get all the main menu links
	$tree_data = menu_tree_all_data('main-menu');
	$menu_links = menu_tree_output($tree_data);

	// Initialize some variables to prevent errors
	$output = '';
	$sub_menu = '';

	foreach ($menu_links as $key => $link) {
		// Add special class needed for Foundation dropdown menu to work
		!empty($link['#below']) ? $link['#attributes']['class'][] = 'has-dropdown' : '';

		// Render top level and make sure we have an actual link
		if (!empty($link['#href'])) {
			$options = array('html' => true);
			foreach ($vars['links'] as $vl) {
				if ($vl['href'] == $link['#href'] && array_key_exists('attributes', $vl) && in_array('active', $vl['attributes']['class'])) {
					$options['attributes']['class'] = array('active');
				}
			}
			$output .= '<li' . drupal_attributes($link['#attributes']) . '>';
			$output .= l('<span>' . $link['#title'] . '</span>', $link['#href'], $options);
			// Duplicate entry for mobile
			$sub_menu .= '<li class="mobile-nav">' . l($link['#title'], $link['#href']) . '</li>';
			// Get sub navigation links if they exist
			foreach ($link['#below'] as $key => $sub_link) {
				if (!empty($sub_link['#href'])) {
					$sub_menu .= '<li>' . l($sub_link['#title'], $sub_link['#href']) . '</li>';
				}
			}
			$output .= !empty($link['#below']) ? '<ul class="dropdown">' . $sub_menu . '</ul>' : '';

			// Reset dropdown to prevent duplicates
			unset($sub_menu);
			$sub_menu = '';

			$output .= '</li>';
		}
	}
	return '<ul>' . $output . '</ul>';
}

function kentcoba_links__menu_secondary_nav(&$vars) {
	$menu = '';
	$menu_links = menu_tree_output(menu_tree_all_data('menu-secondary-nav'));

	foreach ($menu_links as $link) {
		if (!empty($link['#href'])) {
			$link['#attributes']['class'] = array('button', 'round');
			$menu .= l($link['#title'], $link['#href'], array('attributes' => $link['#attributes']));
		}
	}

	return $menu;
}

function kentcoba_links__menu_tertiary_nav(&$vars) {
	$menu = '';
	$menu_links = menu_tree_output(menu_tree_all_data('menu-tertiary-nav'));

	foreach ($menu_links as $link) {
		if (!empty($link['#href'])) {
			$link['#attributes']['class'] = array('button', 'small', 'round', 'color6');
			$menu .= l($link['#title'], $link['#href'], array('attributes' => $link['#attributes']));
		}
	}

	return $menu;
}

function kentcoba_pager($variables) {
	global $pager_page_array, $pager_total;

	$tags       = $variables['tags'];
	$element    = $variables['element'];
	$parameters = $variables['parameters'];
	$quantity   = $variables['quantity'];

	$pager_middle  = ceil($quantity / 2);
	$pager_current = $pager_page_array[$element] + 1;
	$pager_first   = $pager_current - $pager_middle + 1;
	$pager_last    = $pager_current + $quantity - $pager_middle;
	$pager_max     = $pager_total[$element];

	$i = $pager_first;
	if ($pager_last > $pager_max) {
		$i = $i + ($pager_max - $pager_last);
		$pager_last = $pager_max;
	}
	if ($i <= 0) {
		$pager_last = $pager_last + (1 - $i);
		$i = 1;
	}

	$li_first    = theme('pager_first', array('text' => (isset($tags[0]) ? $tags[0] : t('«')), 'element' => $element, 'parameters' => $parameters));
	$li_previous = theme('pager_previous', array('text' => (isset($tags[1]) ? $tags[1] : t('‹')), 'element' => $element, 'interval' => 1, 'parameters' => $parameters));
	$li_next     = theme('pager_next', array('text' => (isset($tags[3]) ? $tags[3] : t('›')), 'element' => $element, 'interval' => 1, 'parameters' => $parameters));
	$li_last     = theme('pager_last', array('text' => (isset($tags[4]) ? $tags[4] : t('»')), 'element' => $element, 'parameters' => $parameters));

	if ($pager_total[$element] > 1) {
		if ($li_first) {
			$items[] = array(
				'class' => array('arrow'),
				'data' => $li_first,
			);
		}
		if ($li_previous) {
			$items[] = array(
				'class' => array('pager-previous'),
				'data' => $li_previous,
			);
		}

		if ($i != $pager_max) {
			if ($i > 1) {
				$items[] = array(
					'class' => array('pager-ellipsis'),
					'data' => '…',
				);
			}
			for (; $i <= $pager_last && $i <= $pager_max; $i++) {
				if ($i < $pager_current) {
					$items[] = array(
						'class' => array('pager-item'),
						'data' => theme('pager_previous', array('text' => $i, 'element' => $element, 'interval' => ($pager_current - $i), 'parameters' => $parameters)),
					);
				}
				if ($i == $pager_current) {
					$items[] = array(
						'class' => array('current'),
						// 'data' => $i,
						'data' => theme('pager_next', array('text' => $i, 'element' => $element, 'interval' => ($i - $pager_current), 'parameters' => $parameters)),
					);
				}
				if ($i > $pager_current) {
					$items[] = array(
						'class' => array('pager-item'),
						'data' => theme('pager_next', array('text' => $i, 'element' => $element, 'interval' => ($i - $pager_current), 'parameters' => $parameters)),
					);
				}
			}
			if ($i < $pager_max) {
				$items[] = array(
					'class' => array('pager-ellipsis'),
					'data' => '…',
				);
			}
		}

		if ($li_next) {
			$items[] = array(
				'class' => array('pager-next'),
				'data' => $li_next,
			);
		}
		if ($li_last) {
			$items[] = array(
				'class' => array('arrow'),
				'data' => $li_last,
			);
		}

		return theme('item_list', array(
			'items' => $items,
			'attributes' => array('class' => array('pagination')),
		));
	}
}

function kentcoba_views_pre_render(&$view) {
	if ($view->name == 'news' && $view->current_display == 'block') {
		$news = array();
		$i = 0;
		foreach ($view->result as $r) {
			$index = floor($i / 5);

			$item = $r->_field_data['nid']['entity'];
			$date = new DateTime($item->ks_date['und'][0]['value']);

			$obj = new stdClass();
			$obj->img_url = isset($item->ks_image['und'][0]['uri']) ? file_create_url($item->ks_image['und'][0]['uri']) : '';
			$obj->link = $item->ks_link['und'][0]['safe_value'];
			$obj->link_target = ($item->ks_is_external['und'][0]['value'] == 1) ? '_blank' : '';
			$obj->title = $item->title;
			$obj->display_link = ksGetDisplayLink($obj->link);
			$obj->date = $date->format('m/d/Y');

			$news[$index][] = $obj;
			$i++;
		}
		$view->news_json = json_encode($news);
	}

	if ($view->name == 'home_page_video' && $view->current_display == 'home_page_video_block') {
		$result = $view->result[0];
		$view->data = array(
			'title' => $result->node_title,
			'body' => $result->field_body[0]['raw']['safe_value'],
			'image' => file_create_url($result->field_field_image[0]['raw']['uri']),
			'video' => $result->field_field_video_embed[0]['raw']['value'],
		);
	}
}

function kentcoba_views_post_render(&$view) {
	if ($view->display_handler->plugin_name == 'block' && $view->current_display != 'degree_blurb_block') {
		$name = $view->human_name;

		if ($view->current_display == 'why_block') {
			$dept = ucwords($view->args[0]);
			$name = "Why {$dept}?";
		} else if (!empty($view->display_handler->options['block_description'])) {
			$name = $view->display_handler->options['block_description'];
		}

		$view->display_handler->output = "<a name=\"{$name}\"></a>" . $view->display_handler->output;
	}
}

function kentcoba_theme() {
	return array(
		'main_menu' => array(
			'variables' => array(
				'main_menu' => null,
				'site_name' => null,
			),
			'file' => 'shared/main_menu.php',
		),
	);
}

function kentcoba_main_menu($variables = null) {}

function ksGenerateSideNav($links, $label) {
	if (empty($label)) {
		$label = 'Navigation';
	}

	$markup = '';

	if (!empty($links)) {
		$markup = '<h2 class="subheader dotted-after dotted-color5">' . $label . '</h2>';
		$markup .= '<ul class="side-nav">';

		foreach ($links as $link) {
			$text = titlecase(unslugify($link['text']));
			$markup .= "<li><a href=\"#{$link['anchor']}\" data-magellan-arrival=\"{$link['anchor']}\">{$text}</a></li>";
		}

		$markup .= '</ul>';
	}

	return $markup;
}

function ksExtractBlockMarkup($content) {
	$blocks = array();
	$markup = '';

	foreach ($content as $key => $value) {
		if (preg_match('/^(views|block)/', $key)) {
			$blocks[$value['#weight']] = $value['#markup'];
		}
	}
	ksort($blocks);

	foreach ($blocks as $block) {
		$markup .= $block;
	}

	return $markup;
}

function ksExtractContentMarkup($content) {
	$markup = '';

	foreach ($content['nodes'] as $node) {
		if (is_array($node) && array_key_exists('#node', $node)) {
			$markup .= $node['#node']->body['und'][0]['value'];
		}
	}

	return $markup;
}

function ksExtractSidebarLabel($content) {
	$label = '';
	foreach ($content['nodes'] as $node) {
		if (is_array($node) && array_key_exists('#node', $node)) {
			if (empty($label)) {
				$label = $node['#node']->field_navigation_label['und'][0]['value'] ? $node['#node']->field_navigation_label['und'][0]['value'] : $node['#node']->title;
			}
		}
	}

	return $label;
}

function ksExtractSidebarLinks($content) {
	$doc = new DOMDocument();
	$loaded = $doc->loadHTML($content);

	$links = array();
	if ($loaded) {
		$xpath = new DOMXPath($doc);
		$elements = $xpath->query('//a[@name]');
		foreach ($elements as $e) {
			$links[] = array(
				'anchor' => $e->getAttribute('name'),
				'text' => !empty($e->nodeValue) ? $e->nodeValue : $e->getAttribute('name'),
			);
		}
	}

	return $links;
}

function ksGenerateFullName($vars) {
	$first_name = array_key_exists('und', $vars['ks_first_name']) ? $vars['ks_first_name']['und'][0]['value'] : $vars['ks_first_name'][0]['value'];
	$last_name  = array_key_exists('und', $vars['ks_last_name']) ? $vars['ks_last_name']['und'][0]['value'] : $vars['ks_last_name'][0]['value'];
	$suffix     = array_key_exists('und', $vars['ks_staff_suffix']) ? $vars['ks_staff_suffix']['und'][0]['value'] : $vars['ks_staff_suffix'][0]['value'];

	$full_name  = '';
	$full_name .= isset($first_name) ? $first_name : '';
	$full_name .= isset($last_name) ? " {$last_name}" : '';
	$full_name .= isset($suffix) ? ", {$suffix}" : '';

	return trim($full_name);
}

function ksGetDisplayLink($link) {
	$display_link = (strpos($link, '//') !== false) ? substr($link, strpos($link, '//') + 2) : $link;
	$display_link = (strpos($display_link, '/') !== false) ? substr($display_link, 0, strpos($display_link, '/')) : $display_link;
	$display_link = preg_replace('/^www\./', '', $display_link);

	return $display_link;
}

function ksGetSaneDate($date) {
	$dt = new DateTime($date['value'], new DateTimeZone($date['timezone_db']));
	$dt->setTimezone(new DateTimeZone($date['timezone']));

	return $dt;
}

if (!function_exists('ksGetSaneStartEndDate')) {
	function ksGetSaneStartEndDate($date) {
		$start = new DateTime($date['value'], new DateTimeZone($date['timezone_db']));
		$start->setTimezone(new DateTimeZone($date['timezone']));

		if (array_key_exists('value2', $date)) {
			$end = new DateTime($date['value2'], new DateTimeZone($date['timezone_db']));
			$end->setTimezone(new DateTimeZone($date['timezone']));
		} else {
			$end = clone $start;
		}

		return array(
			'start' => $start,
			'end'   => $end,
		);
	}
}

function ellipsis($string, $limit) {
	if (strlen($string) <= $limit) {
		return $string;
	}

	return substr($string, 0, $limit) . '...';
}

function slugify($string) {
	return strtolower(str_replace(' ', '-', $string));
}

function titlecase($string) {
	$no_convert = array(
		'of', 'a', 'the', 'and', 'an', 'or', 'nor', 'but', 'is', 'if', 'then', 'else', 'when',
		'at', 'from', 'by', 'on', 'off', 'for', 'in', 'out', 'over', 'to', 'into', 'with',
	);

	$words = explode(' ', $string);
	foreach ($words as $key => $word) {
		if (!in_array($word, $no_convert)) {
			$words[$key] = ucwords($word);
		}
	}

	return implode(' ', $words);
}

function unslugify($string) {
	return str_replace('-', ' ', $string);
}
