var Site = {};
(function($, ns, document, window, mustache, env) {
	var doc = $(document),
		win = $(window),
		templates = {};

	ns.init = function() {
		doc.ready(docReady);
		win.load(winLoaded);
	};

	var eventsForm = function() {
		var list = '<ul id="events-department-dropdown" class="f-dropdown content round" data-dropdown-content>';
		$('#edit-affiliation-filter > option').each(function() {
			if ($(this).attr('selected')) {
				$('#selected-department').html($(this).html()).attr('data-department', $(this).val());
			}
			list += '<li><a href="#" data-department="'+ $(this).val() +'">'+ $(this).html() +'</a></li>';
		});
		list += '</ul>';
		$('#edit-affiliation-filter').replaceWith(list);
	};

	var newsForm = function() {
		var frm = $('#views-exposed-form-news-page').parent();
		var select_list = '<ul id="news-year-dropdown" class="f-dropdown content round" data-dropdown-content>';
		var year_list = '';
		$('#edit-news-year-filter-value-year > option').each(function() {
			if ($(this).attr('selected')) {
				$('#selected-year').html($(this).html()).attr('data-year', $(this).val());
			}
			if ($(this).val().length > 0) {
				year_list = '<li><a href="#" data-year="' + $(this).val() + '">' + $(this).html() + '</a></li>' + year_list;
			}
		});
		year_list = '<li><a href="#" data-year="">All</a></li>' + year_list;
		select_list += year_list;
		select_list += '</ul>';
		$('#edit-news-year-filter-value-year').hide();
		frm.append(select_list);
	};

	var staffForm = function() {
		$('#edit-department').hide();
	};


	var newsMasonry = function(){
		// News listing Masonry
		var newsItems = $('.news-items, .news-summary-wrapper');
		newsItems.imagesLoaded( function() {
			newsItems.masonry({
				columnWidth: '.news-items-sizer',
				itemSelector: '.news-summary',
				gutter: '.news-items-gutter',
				transitionDuration: 0
			});
		});
	};


	var docReady = function() {
		eventsForm();
		newsForm();
		staffForm();
		compileTemplates();
		NewsSliders.setup(".news-slider");
		newsMasonry();

		$(document).delegate('#events-department-dropdown a', 'click', function(e) {
			e.preventDefault();
			var dom_id = $(this).parents('form').attr('data-domid');
			var view   = $(this).parents('.events-filters').attr('data-view-type');
			var monday = new Date($('#event-date-filter').attr('data-monday') * 1000);
			$.ajax({
				url: '/views/ajax',
				type: 'POST',
				data: {
					view_name: 'events',
					view_display_id: view,
					view_dom_id: dom_id,
					affiliation_filter: $(this).attr('data-department'),
					monday: monday.toDateString()
				},
				success: function(data, status, xhr) {
					var response = data[1];
					$(response.selector).replaceWith(response.data);
					eventsForm();
				}
			});
		});

		$(document).delegate('#event-date-filter a', 'click', function(e) {
			e.preventDefault();
			var parent = $(this).parent();
			var dom_id = parent.attr('data-domid');
			var view   = parent.attr('data-view-display');
			var monday = new Date(parent.attr('data-monday') * 1000);
			var day    = monday.getDate();
			var dir    = $(this).attr('data-dir');

			if (dir === 'back') {
				monday.setDate(day - 7);
			} else if (dir === 'forward') {
				monday.setDate(day + 7);
			}

			$.ajax({
				url: '/views/ajax',
				type: 'POST',
				data: {
					view_name: 'events',
					view_display_id: view,
					view_dom_id: dom_id,
					affiliation_filter: $('#selected-department').attr('data-department'),
					monday: monday.toDateString()
				},
				success: function(data, status, xhr) {
					var response = data[1];
					$(response.selector).replaceWith(response.data);
					eventsForm();
				}
			});
		});

		$(document).delegate('#news-department-dropdown a', 'click', function(e) {
			e.preventDefault();

			var frm = $('#views-exposed-form-news-page');
			var loading = frm.siblings('.loading');
			loading.fadeIn();

			var label = $(this).html().length > 30 ? $(this).html().substring(0, 30) + '...' : $(this).html();
			$('#selected-department')
				.attr('data-department', $(this).attr('data-department'))
				.html(label);
			$(this).parents('ul').hide();

			frm.append('<input type="hidden" name="affiliation_filter" value="'+$(this).attr('data-department')+'">');
			frm.find('[name="news_year_filter[value][year]"]').val($('#selected-year').attr('data-year'));
			frm.submit();
		});

		$(document).delegate('#news-year-dropdown a', 'click', function(e) {
			e.preventDefault();

			var frm = $('#views-exposed-form-news-page');
			var loading = frm.siblings('.loading');
			loading.fadeIn();

			$('#selected-year')
				.attr('data-year', $(this).attr('data-year'))
				.html($(this).html());
			$(this).parents('ul').hide();

			frm.append('<input type="hidden" name="affiliation_filter" value="'+$('#selected-department').attr('data-department')+'">');
			frm.find('[name="news_year_filter[value][year]"]').val($('#selected-year').attr('data-year'));
			frm.submit();
		});


		$("#google-appliance-block-form").bind("submit", function(e){
			e.preventDefault();
			var action = $(this).attr("action");
			var string = $(this).find("input[name=search_keys]").val();
			window.location.href = action + "/" + string;
		});

		$('#news-load-more').click(function(e) {
			e.preventDefault();
			var btn    = $(this);
			var dom_id = $('#views-exposed-form-news-page').attr('data-domid');
			var dept   = $('#selected-department').attr('data-department');
			var year   = $('#selected-year').attr('data-year');
			var offset = btn.attr('data-offset');
			var total  = btn.attr('data-rows');

			btn.find(".loading").fadeIn();

			$.ajax({
				url: '/views/ajax',
				type: 'POST',
				data: {
					view_name: 'news',
					view_display_id: 'page',
					view_dom_id: dom_id,
					news_affiliation_filter: dept,
					"news_year_filter[value][year]": year,
					offset: offset
				},
				success: function(data, status, xhr) {
					var response = data[1]; // extract div.news-items from response.data
					var view = $(response.selector);
					var insert = view.find('.news-items');
					var articles = $(response.data).find('article');
					insert.append(articles);
					articles.css("visibility", "hidden").imagesLoaded(function() {
						insert.masonry('appended', articles);
						articles.css("visibility", "visible");
					});
					var current_count = parseInt(offset, 10) + articles.length;
					btn.attr('data-offset', current_count);
					btn.find(".loading").fadeOut();
					if (current_count >= total) {
						btn.hide();
					}
				}
			});

		});

		$(document).delegate('#staff-department-dropdown a', 'click', function(e) {
			e.preventDefault();
			var dept_filter = $(this).data('department');
			var name_filter = $('#selected-names').data('namegroup');

			var label = $(this).html().length > 30 ? $(this).html().substring(0, 30) + '...' : $(this).html();
			$('#selected-department')
				.attr('data-department', dept_filter)
				.html(label);

			window.location.href = env + '?department=' + dept_filter + '&name_group=' + name_filter;
		});

		$(document).delegate('#staff-names-dropdown a', 'click', function(e) {
			e.preventDefault();
			var dept_filter = $('#selected-department').data('department');
			var name_filter = $(this).data('namegroup');
			window.location.href = env + '?department=' + dept_filter + '&name_group=' + name_filter;
		});

		$('#keyword-search').on('keypress', function(e) {
			if (e.keyCode === 13) {
				e.preventDefault();
				var keyword = $(this).val();
				window.location.href = '/search/node/' + keyword;
			}
		});

		$('#user-login :submit').show().addClass('button round color2');

		equalheight('.current-news article');

		// Homepage sliders
		$('.flexslider-home-main').flexslider({
			directionNav: false
		});
		$('.flexslider-home-secondary').flexslider({
			directionNav: false
		});
		$('.flexslider-home-events').flexslider({
			animation: "slide",
			controlsContainer: ".flexslider-home-events-container",
			directionNav: false
		});
		$('.flexslider-home-events-small').flexslider({
			animation: "slide",
			controlsContainer: ".flexslider-home-events-container-small",
			controlNav: false,
			directionNav: true
		});

		// Search Toggle
		$(".top-bar .button-search").on("click", function(e){
			e.preventDefault();
			$(this).toggleClass("active");
			$(this).siblings(".block-search").toggleClass("active");
		});


		// Smooth scrolls
		$('a[href*=#]:not([href=#])').click(function() {
			if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
				var target = $('[name="' + this.hash.slice(1) + '"]');
				target = target.length ? target : $('[name="' + this.hash.slice(1) +'"]');
				if (target.length) {
					$('html,body').animate({
						scrollTop: target.offset().top
					}, 1000);
					return false;
				}
			}
		});

		// Foundation Accordion
		$(".accordion").on("click", "section", function (event) {

			var currentBtn = $(this).find("[data-section-button]");
			if($(this).hasClass("active")){
				currentBtn.html("Expand");
			}else{
				currentBtn.html("Close");
			}

			$(this).siblings("section").find("[data-section-button]").html("Expand");
		});


		// Foundation Magellan
		$("a[name]").each(function(){
			if($(this).parent().is("h1, h2, h3, h4, h5, h6, article")){
				$(this).parents("h1:first, h2:first, h3:first, h4:first, h5:first, h6:first, article:first").attr("data-magellan-destination", $(this).attr("name"));
			}else{
				$(this).attr("data-magellan-destination", $(this).attr("name"));
			}
		});

		// Foundation
		jQuery(document).foundation();


		// Foundation Hack for 2 topbars
		// Very Hacky
		$(".toggle-topbar a").on("click", function(e){
			e.preventDefault();
			$(this).parents(".top-bar").toggleClass("expanded-alone");
		});

	};


	win.scroll(function(){
		var m = $('div[data-magellan-expedition="fixed"]')
		var mh = m.height();
		var f = $('#coba-footer').height() + $('#kent-footer').height();
		var h = $(document).height();
		var st = $(window).scrollTop();

		// created an offset to keep it from butting against the footer
		var o = 350;

		if( (mh + f + o) >= (h - st)){
			m.css('top', ((h - st) - (mh+ f+ o)));
		}else{
			m.css('top', 25)
		}
	});


	$(document).bind('DOMNodeInserted', function() {
		$('.flexslider-home-events').flexslider({
			animation: "slide",
			controlsContainer: ".flexslider-home-events-container",
			directionNav: false
		});
	});


	$(window).resize(function(){
		equalheight('.current-news article');
	});

	/*
	 News Slider
	 */
	var NewsSliders = (function() {
		var defaultConfig = { };

		var init = function(selector) {
			$(window).load(function() {
				$(selector).each(register);
			});
			return true;
		};

		var register = function(){
			var slider = $(this),
				sliderItems = slider.children("li"),
				currentPage = 1,
				allItems = window[slider.data("items")],
				totalPage = allItems.length,
				current = slider.find(".news-slider-current"),
				total = slider.find(".news-slider-total"),
				prevButton = slider.find(".js-role-previous-page"),
				nextButton = slider.find(".js-role-next-page");

			createNav(totalPage);

			function createNav(totalPage){
				total.html(totalPage);

				prevButton.on("click", prevPage);
				nextButton.on("click", nextPage);
			}

			function updateNav(newPage){
				current.html(newPage);
			}

			function updatePages(newPage){
				slider.addClass("active");

				sliderItems.each(function(index){

					if(index!=0){ // skip first since its the navigation
						if(typeof allItems[newPage-1][index-1] != 'undefined'){
							var that = $(this);

							// alert(this);

							if(allItems[newPage-1][index-1].img_url != ""){
								// Preload before animating
								$("<img src='"+allItems[newPage-1][index-1].img_url+"'>").on("load", function(){
									// Remove old news
									removeOld(index, that, newPage);

									// Display new news
									displayNew(index, that, newPage);
								});
							}else{
								// Remove old news
								removeOld(index, that, newPage);

								// Display new news
								displayNew(index, that, newPage);
							}
						}
					}

				});


				if(!Modernizr.cssanimations){
					sliderItems.children("article.old").remove();
					slider.removeClass("active");
				}
			}

			// Remove old news
			function removeOld(index, that, newPage){
				var oldItem = that.children("article:not(.new)");

				if(Modernizr.cssanimations){
					oldItem.addClass("old")
						.on("webkitAnimationEnd oanimationend msAnimationEnd animationend", function(){
							$(this).remove();
						});
				}else{
					oldItem.remove();
				}
			};

			// Display new news
			function displayNew(index, that, newPage){
				var newItem = $(templates.tmplNews.render(allItems[newPage-1][index-1]));

				if(Modernizr.cssanimations){
					newItem.appendTo(that)
						.on("webkitAnimationEnd oanimationend msAnimationEnd animationend", function(){
							$(this).removeClass("new");
							slider.removeClass("active");
						});
				}else{
					newItem.appendTo(that).removeClass("new");
					slider.removeClass("active");
				}
			}

			function nextPage(e){
				if(!slider.hasClass("active")){
					currentPage = (currentPage === totalPage) ? 1 : currentPage+1;
					updateNav(currentPage);
					updatePages(currentPage);
				}

				e.preventDefault();
			}

			function prevPage(e){
				if(!slider.hasClass("active")){
					currentPage = (currentPage === 1) ? totalPage : currentPage-1;
					updateNav(currentPage);
					updatePages(currentPage);
				}

				e.preventDefault();
			}
		};

		return {
			setup : init
		};

	})();


	equalheight = function(container){

		var currentTallest = 0,
		     currentRowStart = 0,
		     rowDivs = new Array(),
		     $el,
		     topPosition = 0;
		 $(container).each(function() {

		   $el = $(this);
		   $($el).height('auto')
		   topPostion = $el.position().top;

		   if (currentRowStart != topPostion) {
		     for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
		       rowDivs[currentDiv].height(currentTallest);
		     }
		     rowDivs.length = 0; // empty the array
		     currentRowStart = topPostion;
		     currentTallest = $el.height();
		     rowDivs.push($el);
		   } else {
		     rowDivs.push($el);
		     currentTallest = (currentTallest < $el.height()) ? ($el.height()) : (currentTallest);
		  }
		   for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
		     rowDivs[currentDiv].height(currentTallest);
		   }
		 });
	}


	/**
	 * Compiles all js templates into native objects
	 */
	var compileTemplates = function() {
		$('.jstemplate').each(function(idx, node) {
			var obj = $(node);
			templates[obj.attr('id')] = mustache.compile(obj.html());
		});
	};


	var winLoaded = function() {};

	ns.init();
})(jQuery, Site, document, window, Hogan, window['env']);
