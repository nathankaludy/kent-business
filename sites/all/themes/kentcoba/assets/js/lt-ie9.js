jQuery(function(){
	jQuery(".columns:last-child").addClass("last-child");
	jQuery(".footer-contact address:nth-child(3n + 1)").addClass("nth-child-3n1");
	jQuery(".footer-contact address:nth-last-child(-n + 3)").addClass("nth-last-child-n3");
	jQuery(".even-2, .even-3, .even-4, .even-5, .even-6, .even-7, .even-8, .even-9, .even-10").find("*:last-child").addClass("last-child");
	jQuery(".list-profiles .profile-summary:nth-child(4n+1)").addClass("nth-child-4n1");
	jQuery(".list-profiles .profile-summary:nth-child(4n+2)").addClass("nth-child-4n2");
});